
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [backoffice].[pReportManagerLekmerGetVocherInfoByBackIdOrVocherCode]
	@ValidFrom		datetime = NULL,
	@ValidTo		datetime = NULL,	
	@ChannelGroupId	int = NULL,
	@CreatedBy		nvarchar(50) = NULL,
	@BatchId		int = NULL,
	@VoucherCode	nvarchar(50) = NULL,
	@OrderId		int = NULL
AS
begin
	set nocount on
		
		-- ställ om till dagar 2359 
		
		--set @ChannelGroupId = coalesce(@ChannelGroupId, 12) 
		--set @ValidFrom = coalesce(@ValidFrom, '2009-08-12 00:00:00.000')
		--set @ValidTo = coalesce(@ValidTo, '2099-08-12 00:00:00.000')
		--set @BatchId = coalesce(@BatchId, )
		
		(select 
			vi.VoucherInfoId, 
			vi.ValidFrom, 
			coalesce(vi.ValidTo, v.ValidTo) as ValidTo, 
			vi.Quantity, 
			d.DiscountType, 
			DiscountValue,
			vi.DiscountTitle as CustomerNo,
			vi.[Description],
			vi.IsActive, 
			c.[Description] as Channel,
			OriginalStartQuantityPerCode, 
			vi.CreatedBy,
			v.VoucherCode, 
			v.QuantityLeft,
			l.OrderId
		from 
			product.tVoucherInfo vi
			inner join product.tVoucher v
				on vi.VoucherInfoId = v.VoucherInfoId
			inner join product.tChannelGroup c
				on c.ChannelGroupId = vi.ChannelGroupId
			inner join product.tDiscountType d
				on vi.DiscountTypeId = d.DiscountTypeId
			left join product.tVoucherLog l
				on l.VoucherId = v.VoucherId
		where
			--vi.ChannelGroupId = ( select SiteId from product.tChannelGroup where ChannelGroupId = 12)
			--and (vi.ValidFrom > @ValidFrom)
			--and (v.ValidTo < @ValidTo)

			(@BatchId is null or vi.VoucherInfoId = @BatchId) 
			and
			(@VoucherCode is null or v.VoucherCode = @VoucherCode)
			and
			(@ValidFrom is null or vi.ValidFrom > @ValidFrom)
			and
			(@ValidTo is null or ((v.ValidTo < @ValidTo) or (vi.ValidTo < @ValidTo)))
			and
			(@CreatedBy is null or vi.CreatedBy = @CreatedBy)
			and
			(@OrderId is null or l.OrderId = @OrderId)
			AND vi.ChannelGroupId IN (1,2,3,4,13,16)
		)
		
	UNION ALL
		
		(select 
			vi.VoucherInfoId, 
			vi.ValidFrom, 
			coalesce(vi.ValidTo, v.ValidTo) as ValidTo, 
			vi.Quantity, 
			d.DiscountType, 
			DiscountValue,
			vi.DiscountTitle as CustomerNo,
			vi.[Description],
			vi.IsActive, 
			c.[Description] as Channel,
			OriginalStartQuantityPerCode, 
			vi.CreatedBy,
			v.VoucherCode, 
			v.QuantityLeft,
			l.OrderId
		from 
			archive.tVoucherInfo vi
			inner join archive.tVoucher v
				on vi.VoucherInfoId = v.VoucherInfoId
			inner join product.tChannelGroup c
				on c.ChannelGroupId = vi.ChannelGroupId
			inner join product.tDiscountType d
				on vi.DiscountTypeId = d.DiscountTypeId
			left join product.tVoucherLog l
				on l.VoucherId = v.VoucherId
		where
			--vi.ChannelGroupId = ( select SiteId from product.tChannelGroup where ChannelGroupId = 12)
			--and (vi.ValidFrom > @ValidFrom)
			--and (v.ValidTo < @ValidTo)

			(@BatchId is null or vi.VoucherInfoId = @BatchId) 
			and
			(@VoucherCode is null or v.VoucherCode = @VoucherCode)
			and
			(@ValidFrom is null or vi.ValidFrom > @ValidFrom)
			and
			(@ValidTo is null or ((v.ValidTo < @ValidTo) or (vi.ValidTo < @ValidTo)))
			and
			(@CreatedBy is null or vi.CreatedBy = @CreatedBy)
			and
			(@OrderId is null or l.OrderId = @OrderId)
			AND vi.ChannelGroupId IN (1,2,3,4,13,16)
		)
end

GO
