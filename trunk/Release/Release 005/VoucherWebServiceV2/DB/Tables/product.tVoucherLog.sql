CREATE TABLE [product].[tVoucherLog]
(
[VoucherUsedId] [int] NOT NULL IDENTITY(1, 1),
[VoucherId] [int] NULL,
[UsageDate] [datetime] NOT NULL,
[OrderId] [int] NULL,
[AmountUsed] [decimal] (18, 0) NULL
) ON [PRIMARY]
ALTER TABLE [product].[tVoucherLog] ADD 
CONSTRAINT [PK_tVoucherLog] PRIMARY KEY CLUSTERED  ([VoucherUsedId]) ON [PRIMARY]
GO
