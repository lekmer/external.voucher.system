CREATE TABLE [product].[tVoucherProlongLog]
(
[VoucherInfoId] [int] NOT NULL,
[OldValidTo] [datetime] NOT NULL,
[NewValidTo] [datetime] NOT NULL,
[ChangeDate] [datetime] NULL,
[ChangeBy] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Status] [nvarchar] (20) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
