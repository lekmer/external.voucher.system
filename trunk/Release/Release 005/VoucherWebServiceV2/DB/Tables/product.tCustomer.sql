CREATE TABLE [product].[tCustomer]
(
[CustomerId] [int] NOT NULL,
[Value] [nvarchar] (320) COLLATE Finnish_Swedish_CI_AS NULL,
[Type] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [product].[tCustomer] ADD 
CONSTRAINT [PK_tCustomer] PRIMARY KEY CLUSTERED  ([CustomerId]) ON [PRIMARY]
GO
