CREATE TABLE [product].[tVoucherCustomer]
(
[CustomerId] [int] NOT NULL,
[VoucherId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [product].[tVoucherCustomer] ADD
CONSTRAINT [FK_tVoucherCustomer_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [product].[tCustomer] ([CustomerId])
ALTER TABLE [product].[tVoucherCustomer] ADD
CONSTRAINT [FK_tVoucherCustomer_tVoucher] FOREIGN KEY ([VoucherId]) REFERENCES [product].[tVoucher] ([VoucherId])
ALTER TABLE [product].[tVoucherCustomer] ADD 
CONSTRAINT [PK_tVoucherCustomer] PRIMARY KEY CLUSTERED  ([CustomerId], [VoucherId]) ON [PRIMARY]
GO
