﻿namespace Voucher
{
	// product.tVoucherInfo
	public class VoucherBatch
	{
		public string ValidFrom { get; set; }
		public string ValidTo { get; set; }
		public string Prefix { get; set; }
		public string Suffix { get; set; }
		public int Quantity { get; set; }
		public int DiscountTypeId { get; set; }
		public decimal DiscountValue { get; set; }
		public decimal IsActive { get; set; }
		public string ApplicationName { get; set; }
		public string DiscountTitle { get; set; }
		public string Description { get; set; }
		public int NoOfToken { get; set; }
		public int OriginalStartQuantityPerCode { get; set; }
		public int ValidInDays { get; set; }
	}
}