﻿using System;

namespace Voucher
{
	[Serializable]
	public class SimpleVoucherBatch
	{
		public int Id { get; set; }
		public int Quantity { get; set; }
	}
}