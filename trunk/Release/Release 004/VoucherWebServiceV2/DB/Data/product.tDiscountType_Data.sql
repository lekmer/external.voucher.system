SET IDENTITY_INSERT [product].[tDiscountType] ON
INSERT INTO [product].[tDiscountType] ([DiscountTypeId], [DiscountType]) VALUES (3, N'GiftCard')
SET IDENTITY_INSERT [product].[tDiscountType] OFF
SET IDENTITY_INSERT [product].[tDiscountType] ON
INSERT INTO [product].[tDiscountType] ([DiscountTypeId], [DiscountType]) VALUES (1, N'Percentage')
INSERT INTO [product].[tDiscountType] ([DiscountTypeId], [DiscountType]) VALUES (2, N'Amount')
SET IDENTITY_INSERT [product].[tDiscountType] OFF
