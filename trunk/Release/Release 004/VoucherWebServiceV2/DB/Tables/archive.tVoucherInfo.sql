CREATE TABLE [archive].[tVoucherInfo]
(
[VoucherInfoId] [int] NOT NULL,
[ValidFrom] [datetime] NOT NULL,
[ValidTo] [datetime] NULL,
[Prefix] [varchar] (32) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Suffix] [varchar] (32) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Quantity] [int] NOT NULL,
[DiscountTypeId] [int] NOT NULL,
[DiscountValue] [decimal] (18, 0) NOT NULL,
[IsActive] [bit] NOT NULL,
[ChannelGroupId] [int] NOT NULL,
[DiscountTitle] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NbrOfToken] [int] NOT NULL,
[OriginalStartQuantityPerCode] [int] NOT NULL,
[ValidInDays] [int] NULL,
[CreatedBy] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[CategoryId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [archive].[tVoucherInfo] ADD CONSTRAINT [PK_tVoucherInfo] PRIMARY KEY CLUSTERED  ([VoucherInfoId]) ON [PRIMARY]
GO
ALTER TABLE [archive].[tVoucherInfo] ADD CONSTRAINT [FK_tVoucherInfo_tDiscountType] FOREIGN KEY ([DiscountTypeId]) REFERENCES [product].[tDiscountType] ([DiscountTypeId])
GO
