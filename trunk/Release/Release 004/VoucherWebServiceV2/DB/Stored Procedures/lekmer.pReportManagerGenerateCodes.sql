SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pReportManagerGenerateCodes]
	@ValidFrom		datetime,
	@ValidTo		datetime,
	--@Prefix			nvarchar(50) = NULL,
	--@Suffix			nvarchar(50) = NULL,
	@Quantity		int,
	@DiscountTypeId	int, 
	@DiscountValue	decimal,
	--@IsActive		int,
	@ChannelGroupId	int,
	@Description	nvarchar(250) = NULL,
	@DiscountTitle	nvarchar(250) = NULL,
	--@NbrOfToken		int,
	@OriginalStartQuantityPerCode	int, 
	--@ValidInDays	int,
	@Username		nvarchar(50),
	@Password		varchar(50),
	@VoucherCategoryId int
AS
begin
	set nocount on
	
	DECLARE @tmpTable TABLE (BatchId int not null)
	DECLARE @BatchId int
	DECLARE	@Hexbin varbinary(max)
	DECLARE @tmpPassword nvarchar(50)
	DECLARE @ValidateRestrictions nvarchar(70)
	DECLARE @MaximumAmount decimal
	DECLARE @MaximumPercantage int
	
	begin try
		begin transaction

		set @ValidFrom = replace(cast(convert(varchar, @ValidFrom, 102) as varchar), '.', '-') + ' 00:00:00'
		set @ValidTo = replace(cast(convert(varchar, @ValidTo, 102) as varchar), '.', '-') + ' 00:00:00'
		set @ValidTo = DATEADD(SS,86399,@ValidTo)
		
		set @MaximumAmount = 0
		set @MaximumPercantage = 0
		
		if @DiscountTypeId = 1
		begin
			set @MaximumPercantage = @DiscountValue
		end
		else
		begin
			set @MaximumAmount = @DiscountValue
		end
		
		
		-- Authenticate user
		set @tmpPassword = (select [Password] from Lekmer.[security].tSystemUser where Username = @Username)
		set @Hexbin = (select (hashbytes('SHA1', @Password)))
		
		set @ValidateRestrictions = (select [user].[fValidateVoucherCreationRestrictions]
				(@Username,@MaximumAmount,@MaximumPercantage,@Quantity,@OriginalStartQuantityPerCode))		
		
	
		if @tmpPassword = (CONVERT(varchar(max), @Hexbin, 2)) -- Password OK
			and (select StatusId from Lekmer.[security].tSystemUser where Username = @Username) = 0 -- Online status OK
			and (@ValidateRestrictions is not null)  -- ValidateRestrictions OK
		begin
		
			-- Create a Batch
			insert into product.tVoucherInfo 
			(
				ValidFrom,
				ValidTo,
				Prefix,
				Suffix,
				Quantity,
				DiscountTypeId, 
				DiscountValue,
				IsActive,
				ChannelGroupId,
				[Description], 
				DiscountTitle,
				NbrOfToken,
				OriginalStartQuantityPerCode,
				CreatedBy,
				CategoryId
			 ) --,ValidInDays)
			output inserted.VoucherInfoId INTO @tmpTable
			values
			(
				@ValidFrom, -- lägg till 00:00:00 -- Convert(varchar, GETDATE(), 112)
				@ValidTo,
				'',
				'',
				@Quantity, -- Quantity hur många koder som ska skapas
				@DiscountTypeId,
				@DiscountValue,
				1,
				@ChannelGroupId,
				@Description,
				@DiscountTitle,
				10,
				@OriginalStartQuantityPerCode,
				@Username,
				@VoucherCategoryId
			)
			
			set @BatchId = (select top 1 BatchId from @tmpTable)
			print @BatchId
			delete from @tmpTable
			
			exec [backoffice].[pVoucherGenerateCodes] @BatchId,@Quantity,null,null 		
		end
		
		commit	
	end try
	begin catch
		if @@trancount > 0 rollback transaction
		
		insert into [product].[tVoucherCreationLog](VoucherInfoId, [Date], CreatedBy, [Message])
		values(@BatchId, GETDATE(), @Username, ERROR_MESSAGE() + ' ' + ERROR_PROCEDURE())
		

	end catch		 
end

GO
