SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [backoffice].[pVoucherCountGetByVoucherInfo]
	@VoucherInfoId	INT
AS
BEGIN
	SELECT
		COUNT(*)
	FROM
		[product].[tVoucher]
	WHERE
		VoucherInfoId = @VoucherInfoId
END
GO
