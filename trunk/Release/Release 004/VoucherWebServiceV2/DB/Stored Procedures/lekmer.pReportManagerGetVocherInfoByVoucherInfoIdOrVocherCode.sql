SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pReportManagerGetVocherInfoByVoucherInfoIdOrVocherCode]
	@ValidFrom		datetime = NULL,
	@ValidTo		datetime = NULL,	
	@ChannelGroupId	int = NULL,
	@CreatedBy		nvarchar(50) = NULL,
	@BatchId		int = NULL,
	@VoucherCode	nvarchar(50) = NULL,
	@OrderId		int = NULL,
	@VoucherCategoryId int = NULL
AS
begin
	set nocount on
		
		--set @ChannelGroupId = coalesce(@ChannelGroupId, 12) 
		--set @ValidFrom = coalesce(@ValidFrom, '2009-08-12 00:00:00.000')
		--set @ValidTo = coalesce(@ValidTo, '2099-08-12 00:00:00.000')
		--set @BatchId = coalesce(@BatchId, )
		
		(select 
			vi.VoucherInfoId, 
			vi.ValidFrom, 
			coalesce(vi.ValidTo, v.ValidTo) as ValidTo, 
			vi.Quantity, 
			d.DiscountType, 
			DiscountValue,
			vi.DiscountTitle as CustomerNo,
			vi.[Description],
			vi.IsActive, 
			c.[Description] as Channel,
			OriginalStartQuantityPerCode, 
			vi.CreatedBy,
			v.VoucherCode, 
			v.QuantityLeft,
			l.OrderId,
			ct.Title
		from 
			product.tVoucherInfo vi
			inner join product.tVoucher v
				on vi.VoucherInfoId = v.VoucherInfoId
			inner join product.tChannelGroup c
				on c.ChannelGroupId = vi.ChannelGroupId
			inner join product.tDiscountType d
				on vi.DiscountTypeId = d.DiscountTypeId
			left join product.tVoucherLog l
				on l.VoucherId = v.VoucherId
			left join product.tVoucherCategoryType ct
				on ct.VoucherCategoryTypeId = vi.CategoryId
		where
			--vi.ChannelGroupId = ( select SiteId from product.tChannelGroup where ChannelGroupId = 12)
			--and (vi.ValidFrom > @ValidFrom)
			--and (v.ValidTo < @ValidTo)

			(@BatchId is null or vi.VoucherInfoId = @BatchId) 
			and
			(@VoucherCode is null or v.VoucherCode = @VoucherCode)
			and
			(@ValidFrom is null or vi.ValidFrom > @ValidFrom)
			and
			(@ValidTo is null or ((v.ValidTo < @ValidTo) or (vi.ValidTo < @ValidTo)))
			and
			(@CreatedBy is null or vi.CreatedBy = @CreatedBy)
			and
			(@OrderId is null or l.OrderId = @OrderId)
			and
			(@VoucherCategoryId is null or vi.CategoryId = @VoucherCategoryId)
			and
			(vi.ChannelGroupId between 1 and 4
				or vi.ChannelGroupId = 16 -- NL
				or vi.ChannelGroupId = 13)
		)
		
	UNION ALL
		
		(select 
			vi.VoucherInfoId, 
			vi.ValidFrom, 
			coalesce(vi.ValidTo, v.ValidTo) as ValidTo, 
			vi.Quantity, 
			d.DiscountType, 
			DiscountValue,
			vi.DiscountTitle as CustomerNo,
			vi.[Description],
			vi.IsActive, 
			c.[Description] as Channel,
			OriginalStartQuantityPerCode, 
			vi.CreatedBy,
			v.VoucherCode, 
			v.QuantityLeft,
			l.OrderId,
			ct.Title
		from 
			archive.tVoucherInfo vi
			inner join archive.tVoucher v
				on vi.VoucherInfoId = v.VoucherInfoId
			inner join product.tChannelGroup c
				on c.ChannelGroupId = vi.ChannelGroupId
			inner join product.tDiscountType d
				on vi.DiscountTypeId = d.DiscountTypeId
			left join product.tVoucherLog l
				on l.VoucherId = v.VoucherId
			left join product.tVoucherCategoryType ct
				on ct.VoucherCategoryTypeId = vi.CategoryId
		where
			--vi.ChannelGroupId = ( select SiteId from product.tChannelGroup where ChannelGroupId = 12)
			--and (vi.ValidFrom > @ValidFrom)
			--and (v.ValidTo < @ValidTo)

			(@BatchId is null or vi.VoucherInfoId = @BatchId) 
			and
			(@VoucherCode is null or v.VoucherCode = @VoucherCode)
			and
			(@ValidFrom is null or vi.ValidFrom > @ValidFrom)
			and
			(@ValidTo is null or ((v.ValidTo < @ValidTo) or (vi.ValidTo < @ValidTo)))
			and
			(@CreatedBy is null or vi.CreatedBy = @CreatedBy)
			and
			(@OrderId is null or l.OrderId = @OrderId)
			and
			(@VoucherCategoryId is null or vi.CategoryId = @VoucherCategoryId)
			and
			(vi.ChannelGroupId between 1 and 4
				or vi.ChannelGroupId = 16 -- NL
				or vi.ChannelGroupId = 13)
		)
end

GO
