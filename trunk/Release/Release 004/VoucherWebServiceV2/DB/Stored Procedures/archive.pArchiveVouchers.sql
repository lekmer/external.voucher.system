SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [archive].[pArchiveVouchers]

AS
BEGIN
	SET NOCOUNT ON
		
		DECLARE @ArchivingSuccessful BIT
		DECLARE @Date DATETIME
		SET @Date = DATEADD(day, -14, GETDATE())
		SET @ArchivingSuccessful = 1

		DECLARE @VoucherBatchesToArchive TABLE 
		(
			VoucherInfoId	int	primary key with (ignore_dup_key = on)
		)		

		-- Expired Batches that have validTo date in voucherInfo
		INSERT INTO @VoucherBatchesToArchive (VoucherInfoId)
		SELECT
			VoucherInfoId
		FROM
			product.tVoucherInfo
		WHERE
			ValidTo IS NOT NULL
		AND	ValidTo < @Date


		-- Expired Batches that have validTo date
		INSERT INTO @VoucherBatchesToArchive (VoucherInfoId)
		SELECT
			vi.VoucherInfoId
		FROM
			product.tVoucherInfo vi
		WHERE
			vi.ValidTo IS NULL
		AND	EXISTS 
				(
					SELECT 1 FROM product.tVoucher v WHERE v.VoucherInfoId = vi.VoucherInfoId AND v.ValidTo < @Date
				)
		

		DECLARE @NumberOfVouchersInBatch INT
		DECLARE @VoucherInfoId INT
		DECLARE @BatchArchiveSize INT
		SET @BatchArchiveSize = 1000

		-- Loop through all batches
		DECLARE VoucherInfo CURSOR FAST_FORWARD FOR
		SELECT VoucherInfoId
		FROM @VoucherBatchesToArchive
		OPEN VoucherInfo;
		FETCH NEXT FROM VoucherInfo INTO @VoucherInfoId;
			WHILE @@FETCH_STATUS = 0
			BEGIN
				print '--- Archiving  BatchId ' + cast(@VoucherInfoId as nvarchar(10)) + '---'
				SET @NumberOfVouchersInBatch = (select count(*) from product.tVoucher where VoucherInfoId = @VoucherInfoId)

				BEGIN TRY
					BEGIN TRANSACTION

					-- if voucherid not in archive
					IF @VoucherInfoId NOT IN (SELECT VoucherInfoId FROM archive.tVoucherInfo)
					BEGIN
						-- Copy the batch
						INSERT INTO archive.tVoucherInfo
						(
							VoucherInfoId,
							ValidFrom,
							ValidTo,
							Prefix,
							Suffix,
							Quantity,
							DiscountTypeId,
							DiscountValue,
							IsActive,
							ChannelGroupId,
							DiscountTitle,
							[Description],
							NbrOfToken,
							OriginalStartQuantityPerCode,
							ValidInDays,
							CreatedBy
						)
						SELECT
							VoucherInfoId,
							ValidFrom,
							ValidTo,
							Prefix,
							Suffix,
							Quantity,
							DiscountTypeId,
							DiscountValue,
							IsActive,
							ChannelGroupId,
							DiscountTitle,
							[Description],
							NbrOfToken,
							OriginalStartQuantityPerCode,
							ValidInDays,
							CreatedBy
						FROM
							product.tVoucherInfo
						WHERE
							VoucherInfoId = @VoucherInfoId
					END

						-- WHILE LOOP
					WHILE @NumberOfVouchersInBatch > 0
					BEGIN
						print 'BatchId: ' + cast(@VoucherInfoId as nvarchar(10)) + ' Vouchers left to archive: ' + cast(@NumberOfVouchersInBatch as nvarchar(10))
						-- Copy @BatchArchiveSize to Archive table
						INSERT INTO archive.tVoucher
						(
							VoucherId,
							VoucherInfoId,
							VoucherCode,
							QuantityLeft,
							ValidTo,
							IsActive,
							Reserved,
							AmountLeft
						)
						SELECT TOP (@BatchArchiveSize)
							VoucherId,
							VoucherInfoId,
							VoucherCode,
							QuantityLeft,
							ValidTo,
							IsActive,
							Reserved,
							AmountLeft
						FROM
							product.tVoucher v
						WHERE
							v.VoucherInfoId = @VoucherInfoId
							
						-- Delete these codes from tVocher
						DELETE TOP (@BatchArchiveSize) v
						FROM product.tVoucher v
						WHERE v.VoucherInfoId = @VoucherInfoId

						SET @NumberOfVouchersInBatch = @NumberOfVouchersInBatch - @BatchArchiveSize
					END

					-- Delete the batch (from tVoucherInfo)
					IF @VoucherInfoId IN (SELECT VoucherInfoId FROM archive.tVoucherInfo)
					BEGIN
						DELETE FROM product.tVoucherInfo
						WHERE VoucherInfoId = @VoucherInfoId
						print 'BatchId: ' + cast(@VoucherInfoId as nvarchar(10)) + ' Archived and Deleted from main schema'
					END

					COMMIT TRANSACTION

				END TRY
				BEGIN CATCH
					
					IF @@trancount > 0 ROLLBACK TRANSACTION

					SET @ArchivingSuccessful = 0

					DECLARE @ErrMsg NVARCHAR(2048), @SP NVARCHAR(256), @Severity INT, @State INT
					SELECT @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
					INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
					VALUES('BatchId: ' + cast(@VoucherInfoId as nvarchar(10)) + 'falied', @ErrMsg, GETDATE(), @SP)

				END CATCH
				
				FETCH NEXT FROM VoucherInfo INTO @VoucherInfoId;
			END;

		CLOSE VoucherInfo;
		DEALLOCATE VoucherInfo;

		IF @ArchivingSuccessful = 1
		BEGIN
			print 'Vouchers have been archived successfully'
		END
		ELSE
		BEGIN
			print 'Not all vouchers have been archived successfully, please check you logs for more information'
		END
END
GO
