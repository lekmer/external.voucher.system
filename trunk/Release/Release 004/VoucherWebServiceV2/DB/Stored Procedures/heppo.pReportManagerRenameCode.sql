SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [heppo].[pReportManagerRenameCode]
	@CurrentVoucherCode	nvarchar(200),
	@NewVoucherCode	nvarchar(200),
	@Username		nvarchar(50),
	@Password		varchar(50)	
AS
begin
	set nocount on
	
	declare	@Hexbin varbinary(max)
	declare @tmpPassword nvarchar(50)

	declare @CurrentVoucherId int
	declare @NewVoucherCodeId int
	declare @NumberOfAllowedRenames int

	set @CurrentVoucherId = (select VoucherId from product.tVoucher where VoucherCode = @CurrentVoucherCode)
	set @NewVoucherCodeId = (select VoucherId from product.tVoucher where VoucherCode = @NewVoucherCode)
	set @NumberOfAllowedRenames = 1

	-- Authenticate user
	set @tmpPassword = (select [Password] from Heppo.[security].tSystemUser where Username = @Username)
	set @Hexbin = (select (hashbytes('SHA1', @Password)))


	if @CurrentVoucherId > 0 
		and @NewVoucherCodeId is null
		and @tmpPassword = (CONVERT(varchar(max), @Hexbin, 2)) -- Password OK
		and (select StatusId from Heppo.[security].tSystemUser where Username = @Username) = 0 -- Online status OK
	begin
		begin try
		begin transaction

			Update
				v
			set
				v.VoucherCode = @NewVoucherCode
			from
				product.tVoucher v inner join product.tVoucherInfo vi on v.VoucherInfoId = vi.VoucherInfoId
			Where
				v.VoucherId = @CurrentVoucherId
			and vi.ChannelGroupId in (6,7,8,9,12,14,15)
		
			
			insert into product.tVoucherRenameLog
			(
				VoucherInfoId,
				VoucherId,
				OldVoucherCode,
				NewVoucherCode,
				RenameDate,
				RenamedBy,
				[Status],
				[Message]
			)
			select
				v.VoucherInfoId,
				@CurrentVoucherId,
				@CurrentVoucherCode,
				@NewVoucherCode,
				GETDATE(),
				@Username,
				'SUCCESS',
				null
			from
				product.tVoucher v
			where
				v.VoucherId = @CurrentVoucherId

			
			select 'Voucher [' + @CurrentVoucherCode + '] has been renamed to [' + @NewVoucherCode + '] Successfully'

		commit	
		end try
		begin catch
			if @@trancount > 0 rollback transaction
			
			insert into product.tVoucherRenameLog
			(
				VoucherInfoId,
				VoucherId,
				OldVoucherCode,
				NewVoucherCode,
				RenameDate,
				RenamedBy,
				[Status],
				[Message]
			)
			select
				v.VoucherInfoId,
				@CurrentVoucherId,
				@CurrentVoucherCode,
				@NewVoucherCode,
				GETDATE(),
				@Username,
				'FAILED',
				ERROR_MESSAGE()
			from
				product.tVoucher v
			where
				v.VoucherId = @CurrentVoucherId			
		
			declare @Message nvarchar (max)
			set @Message = (select max([Message]) from product.tVoucherRenameLog)
			select 'Renaming of voucher [' + @CurrentVoucherCode + '] to [' + @NewVoucherCode + '] has failed! ' + @Message

		end catch		 
	end	
	else
	begin
		select 'Renaming of voucher [' + @CurrentVoucherCode + '] to [' + @NewVoucherCode + '] has failed! Check your Vouchers(Voucher with that name already exists) and Username/Password.'
	end
end

GO
