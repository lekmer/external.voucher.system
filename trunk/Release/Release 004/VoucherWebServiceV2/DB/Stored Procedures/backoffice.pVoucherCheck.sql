
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherCheck]
	@VoucherCode			NVARCHAR (50),
	@SiteApplicationName	NVARCHAR(40)
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		DECLARE @SiteId INT
		SET @SiteId = 0
		SELECT @SiteId = siteId FROM product.tSite WHERE SiteTitle = @SiteApplicationName

		IF @SiteId != 0
			BEGIN
				IF EXISTS 
				(SELECT * FROM product.tVoucher v
					INNER JOIN product.tVoucherInfo vi on v.VoucherInfoId = vi.VoucherInfoId
					INNER JOIN product.tChannelGroup chg on chg.ChannelGroupId = vi.ChannelGroupId
				 WHERE
					VoucherCode = @VoucherCode
					AND (QuantityLeft > 0 ) 
					AND ((vi.ValidTo IS NULL OR GETDATE() < (vi.ValidTo)) 
						  AND (v.ValidTo IS NULL OR GETDATE() < (v.ValidTo)))										
					AND (vi.IsActive = 1)
					AND (v.IsActive = 1 OR v.IsActive IS NULL)	
					AND (vi.ValidFrom < GETDATE())
					AND (v.Reserved = 0 OR v.Reserved IS NULL)
					AND chg.SiteId = @SiteId
					-- Voucher 'GiftCard'
					AND (vi.DiscountTypeId <> 3 OR v.AmountLeft > 0)
				 )
					BEGIN
						SELECT 
							vi.DiscountValue, 
							vi.DiscountTypeId,
							vi.VoucherInfoId,
							v.AmountLeft
						FROM 
							product.tVoucher v 
							INNER JOIN product.tVoucherInfo vi ON v.VoucherInfoId = vi.VoucherInfoId
						WHERE 
							v.VoucherCode = @VoucherCode 
					END				
			END
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
					
	END CATCH	 
END
GO
