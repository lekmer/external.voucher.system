SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [user].[fValidateVoucherCreationRestrictions]
(
	@UserId	nvarchar(70),
	@MaximumAmount decimal,
	@MaximumPercantage decimal,
	@MaximumQuantityBatch int,
	@MaximumStartQuantityCode int
)
RETURNS nvarchar(70)
AS
BEGIN
	declare @Return nvarchar(70)
	
	set @Return = (select 
		UserId
	from
		[user].[tVoucherCreationRestrictions] u
	where
		u.UserId = @UserId 
		and ((u.MaximumAmount >= @MaximumAmount) and (u.MaximumPercentage >= @MaximumPercantage))
		and u.MaximumQuantityBatch >= @MaximumQuantityBatch
		and u.MaximumStartQuantityCode >= @MaximumStartQuantityCode)
	

			
	return @return
end
	
GO
