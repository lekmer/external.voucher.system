﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Voucher
{
    [Serializable]
    public class VoucherCheckResult
    {
        private bool _validCode;

        public bool ValidCode
        {
            get { return _validCode; }
            set { _validCode = value; }
        }
    }
}
