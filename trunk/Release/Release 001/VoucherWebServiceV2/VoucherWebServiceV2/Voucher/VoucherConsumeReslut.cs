﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Voucher
{
    public class VoucherConsumeReslut
    {
        public VoucherValueType ValueType { get; set; }

        public bool ValidCode { get; set; }
        public decimal DiscountValue { get; set; }
        public int VoucherbatchId { get; set; }

        public enum VoucherValueType
        {
            Percentage = 1,
            Price = 2
        }
    }
}
