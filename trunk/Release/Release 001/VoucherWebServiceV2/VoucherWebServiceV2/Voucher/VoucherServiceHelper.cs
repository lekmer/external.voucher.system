﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Voucher
{
    public class VoucherServiceHelper
    {

        private static readonly string SqlDbConn = ConfigurationManager.ConnectionStrings["Online"].ToString();

        public VoucherConsumeReslut VoucherCheck(int customerId, string code, string siteId)
        {
            var voucherConsumeReslut = new VoucherConsumeReslut();
            int id = 0;
            if (customerId > 0) { id = customerId; }
            voucherConsumeReslut.ValidCode = false;
            voucherConsumeReslut.ValueType = (VoucherConsumeReslut.VoucherValueType)1;

            using (var connection = new SqlConnection(SqlDbConn))
            {
                var cmd = new SqlCommand("backoffice.pVoucherCheck", connection)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "backoffice.pVoucherCheck",
                    CommandTimeout = 180
                };

                cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@SiteApplicationName", SqlDbType.NVarChar, 40).Value = siteId;


                try
                {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        voucherConsumeReslut.DiscountValue = (decimal)reader.GetValue(0);
                        voucherConsumeReslut.ValueType = (VoucherConsumeReslut.VoucherValueType)(reader.GetInt32(1));
                        voucherConsumeReslut.VoucherbatchId = reader.GetInt32(2);
                        voucherConsumeReslut.ValidCode = true;
                    }

                    return voucherConsumeReslut;
                }
                catch (Exception e)
                {
                    new Log().LogErrorToFile(e);
                }
            }
            return voucherConsumeReslut;
        }

        public VoucherConsumeReslut VoucherConsume(int customerId, string code, int orderId, string siteId)
        {
            var voucherConsumeReslut = new VoucherConsumeReslut();
            int id = 0;
            int oid = 0;
            if (customerId > 0) { id = customerId; }
            if (orderId > 0) { oid = orderId; }

            voucherConsumeReslut.ValidCode = false;
            voucherConsumeReslut.ValueType = (VoucherConsumeReslut.VoucherValueType)1;

            if (VoucherCheck(customerId, code, siteId).ValidCode)
            {
                using (var connection = new SqlConnection(SqlDbConn))
                {
                    var cmd = new SqlCommand("backoffice.[pVoucherConsume]", connection)
                                  {
                                      CommandType = CommandType.StoredProcedure,
                                      CommandText = "backoffice.[pVoucherConsume]",
                                      CommandTimeout = 180
                                  };

                    cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = oid;


                    try
                    {
                        connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            voucherConsumeReslut.DiscountValue = (decimal)reader.GetValue(0);
                            voucherConsumeReslut.ValueType = (VoucherConsumeReslut.VoucherValueType)(reader.GetInt32(1));
                            voucherConsumeReslut.VoucherbatchId = reader.GetInt32(2);
                            voucherConsumeReslut.ValidCode = true;
                        }

                        return voucherConsumeReslut;
                    }
                    catch (Exception e)
                    {
                        new Log().LogErrorToFile(e);
                    }
                }
            
            }
            return voucherConsumeReslut;
        }

        public bool VoucherReserve(string code)
        {
            using (var connection = new SqlConnection(SqlDbConn))
            {
                var cmd = new SqlCommand("backoffice.pVoucherReserve", connection)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "backoffice.pVoucherReserve",
                    CommandTimeout = 180
                };

                cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;

                try
                {
                    connection.Open();

                    if (cmd.ExecuteNonQuery() > 0)
                        return true;
                }
                catch (Exception e)
                {
                    new Log().LogErrorToFile(e);
                }
            }
            return false;
        }

        public bool VoucherRelease(string code)
        {
            using (var connection = new SqlConnection(SqlDbConn))
            {
                var cmd = new SqlCommand("backoffice.pVoucherRelease", connection)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "backoffice.pVoucherRelease",
                    CommandTimeout = 180
                };

                cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;

                try
                {
                    connection.Open();
                    if (cmd.ExecuteNonQuery() > 0)
                        return true;
                    
                }
                catch (Exception e)
                {
                    new Log().LogErrorToFile(e);
                }
            }
            return false;
        }
    }
}
