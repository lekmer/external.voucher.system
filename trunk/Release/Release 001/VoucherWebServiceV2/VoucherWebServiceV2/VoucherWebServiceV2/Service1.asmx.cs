﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Voucher;

namespace VoucherWebServiceV2
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {

        [WebMethod(Description = "Validates a Voucher and returns a VoucherResult object")]
        public VoucherConsumeReslut VoucherCheck(int customerId, string code, string siteId) 
        {
            if (code == null) throw new ArgumentNullException("code");
            if (siteId == null) throw new ArgumentNullException("siteId");

            var voucherServiceHelper = new VoucherServiceHelper();


            VoucherConsumeReslut voucherConsumeReslut = voucherServiceHelper.VoucherCheck(customerId, code, siteId);
            return voucherConsumeReslut;
        }
      
        [WebMethod(Description = "Consumes a Voucher and returns a VoucherResult object")]
        public VoucherConsumeReslut VoucherConsume(int customerId, string code, int orderId, string siteId)
        {
            if (code == null) throw new ArgumentNullException("code");
            if (siteId == null) throw new ArgumentNullException("siteId");

            var voucherServiceHelper = new VoucherServiceHelper();

            VoucherConsumeReslut voucherConsumeReslut = voucherServiceHelper.VoucherConsume(customerId, code, orderId, siteId);
            return voucherConsumeReslut;
        }

        [WebMethod(Description = "Reserves a Voucher")]
        public bool VoucherReserve(string code) // site ?
        {
            if (code == null) throw new ArgumentNullException("code");

            return new VoucherServiceHelper().VoucherReserve(code);
        }

        [WebMethod(Description = "Releases a reserved Voucher")]
        public bool VoucherRelease(string code) // site ?
        {
            if (code == null) throw new ArgumentNullException("code");

            return new VoucherServiceHelper().VoucherRelease(code);
        }
    }
}