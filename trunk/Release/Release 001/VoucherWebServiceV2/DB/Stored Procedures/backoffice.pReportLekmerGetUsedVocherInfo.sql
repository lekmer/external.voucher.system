SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pReportLekmerGetUsedVocherInfo]
	@UsedFrom		datetime = NULL,
	@UsedTo			datetime = NULL,	
	@ChannelGroupId	int = NULL,
	@CreatedBy		nvarchar(50) = NULL,
	@BatchId		int = NULL
AS
begin
	set nocount on
		
		select 		
			vi.VoucherInfoId as BatchId,  
			vi.Quantity, 			
			DiscountValue,
			vi.DiscountTitle,
			vi.[Description],
			c.[Description] as Channel,
			vi.OriginalStartQuantityPerCode, 
			ISNULL(vi.CreatedBy, '') AS CreatedBy,
			d.DiscountType, 
			(vi.Quantity * vi.OriginalStartQuantityPerCode) as TotalAmountOfUsableCodes,			
			(case when vi.DiscountTypeId = 1 then vi.DiscountValue 
					else ((vi.Quantity * vi.OriginalStartQuantityPerCode) * vi.DiscountValue)
					 end) as 'TotalDiscountForBatch (local currency or %)',
			z.TotalUsage as NoOfVouchersUsed,
			(case when vi.DiscountTypeId = 1 then vi.DiscountValue
					else (z.TotalUsage * vi.DiscountValue)
					 end) as 'DiscountUsed (local currency or %)'
		from 
			product.tVoucherInfo vi
			inner join  		
			(			
				select 
					v.VoucherInfoId,
					SUM(l2.[Count]) as TotalUsage
				from
					product.tVoucher v
					inner join 
					(
						select
							l.VoucherId,
							COUNT(*) as 'Count'
						from
							product.tVoucherLog l
						where
							(@UsedFrom is null or l.Used > @UsedFrom)
							and
							(@UsedTo is null or l.Used < @UsedTo)
						group by
							l.VoucherId
					) l2
					on v.VoucherId = l2.VoucherId				
				group by
					v.VoucherInfoId			
			) z
			
				on vi.VoucherInfoId = z.VoucherInfoId
			inner join product.tChannelGroup c
				on c.ChannelGroupId = vi.ChannelGroupId
			inner join product.tDiscountType d
				on vi.DiscountTypeId = d.DiscountTypeId									
			
		where
			(@BatchId is null or vi.VoucherInfoId = @BatchId) 
			and
			(@CreatedBy is null or vi.CreatedBy = @CreatedBy)
			and
			(vi.ChannelGroupId between 1 and 4
				or vi.ChannelGroupId = 13)
		order by
			vi.VoucherInfoId
	 
end
GO
