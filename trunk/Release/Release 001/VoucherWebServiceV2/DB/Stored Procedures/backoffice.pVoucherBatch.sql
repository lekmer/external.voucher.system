SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherBatch]
	@ValidFrom		datetime,
	@ValidTo		datetime,
	@Prefix			varchar (32),
	@Suffix			varchar (32),
	@Quantity		int,
	@DiscountTypeId	int,
	@DiscountValue	decimal,
	@IsActive		bit,
	@SiteId			int,
	@DiscountTitle	nvarchar (250),
	@Description	nvarchar (250),
	@NbrOfToken		int,
	@OriginalStartQuantityPerCode	int,
	@ValidInDays	int
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		declare @Return int
		set @Return = 1

		
		if @Return = 1
		begin
	
			insert into product.tVoucherInfo(ValidFrom, ValidTo, Prefix, Suffix, Quantity, DiscountTypeId,
			DiscountValue, IsActive, SiteId, DiscountTitle, [Description], NbrOfToken, OriginalStartQuantityPerCode,
			ValidInDays)
			select
				(CAST(@ValidFrom AS datetime)),
				(CAST(@ValidTo AS datetime)),
				@Prefix,
				@Suffix,
				@Quantity,
				@DiscountTypeId,
				@DiscountValue,
				@IsActive,
				@SiteId,
				@DiscountTitle,
				@Description,
				@NbrOfToken,
				@OriginalStartQuantityPerCode,
				@ValidInDays
			
			set @Return = 0
		end


	commit transaction
	return @Return
	end try
	begin catch
		set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		return @Return
	end catch		 
end
GO
