SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherConsumeOLD]
	@VoucherCode	nvarchar (50),
	@CustomerId 	int,
	@orderId		int
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		-- Scensum skickar med customer Id när man är inloggad och då funkar itne logiken
		-- då man kan ha en speciell kod länkad till ett customer Id. Så i detta
		-- fall säger at att customer Id alltid är noll till vi reder ut hur logiken ska vara.
		set @CustomerId = 0
		
		declare @Return int
		set @Return = 1
			-- 1 är en aktiv engångsrabatt, 
			-- 2 är en använd engångsrabatt 
			-- 3 är en rabatt som aldrig går ur.
			
		
		if @CustomerId <= 0
		begin
			
			if exists (select 1 
				from product.tOldSystemVoucherCodes
				where 
				(DiscountId = @VoucherCode AND [Status] = 1)
				OR 
				(DiscountId = @VoucherCode AND [Status] = 3))
			begin
				update 
					s 
				set 
					-- if the status is 1 which means it a 1 time code, consume it (change status 2)
					s.[Status] = (case when s.[Status] = 1 then 2 end)
				from
					product.tOldSystemVoucherCodes s
				where
					s.DiscountId = @VoucherCode
				
				set @Return = 0		
			end
			-- if the code was not found in the old table
			-- search in the new system
			if @Return = 1
			begin
				if exists (select 1 
						from product.tVoucher
						where 
						VoucherCode = @VoucherCode
						AND (QuantityLeft > 0 ) AND (ValidTo is null or GETDATE() < (ValidTo)))
				begin
					update 
						h 
					set 
						h.QuantityLeft = (h.QuantityLeft - 1)
					from
						product.tVoucher h
					where
						h.VoucherCode = @VoucherCode
					
					set @Return = 0		
				end
			end
			
		end
		-- if customer id is sent search after a specific code valid only to the id
		-- and see if it is still valid
		else if @CustomerId > 0
		begin
			update
				v
			set
				v.QuantityLeft = (v.QuantityLeft -1)  
			from 
				product.tVoucher v
				inner join product.tVoucherInfo vi
						on v.VoucherInfoId = vi.VoucherInfoId 
				inner join product.tVoucherCustomer vc
						on v.VoucherId = vc.VoucherId
				inner join product.tCustomer c
						on c.CustomerId = vc.CustomerId	
			where 
				vc.CustomerId = @CustomerId
				AND vc.VoucherId = (select VoucherId from product.tVoucher where VoucherCode = @VoucherCode)
				AND (v.QuantityLeft > 0 ) AND (v.ValidTo is null or GETDATE() < (v.ValidTo))
				AND (vi.IsActive = 1)	AND (vi.ValidFrom < GETDATE())
				
			set @Return = 0
				
		end
	
	insert into product.tVoucherLog(VoucherId, OldSystemDiscountCodeId, Used, OrderId)
	select
	(select VoucherId from product.tVoucher where VoucherCode = @VoucherCode),
	(select Id from product.tOldSystemVoucherCodes where DiscountId = @VoucherCode),
	GETDATE(),
	@orderId  -- (case when @orderId is null then 2 end)
		
	commit transaction
	return @Return
	end try
	begin catch
		set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		return @Return
	end catch		 
end
GO
