SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherConsume]
	@VoucherCode	nvarchar (50),
	@CustomerId 	int,
	@orderId		int
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		
		-- Scensum skickar med customer Id när man är inloggad och då funkar itne logiken
		-- då man kan ha en speciell kod länkad till ett customer Id. Så i detta
		-- fall säger at att customer Id alltid är noll till vi reder ut hur logiken ska vara.
		set @CustomerId = 0

		
		if @CustomerId = 0
		begin

			update 
				h 
			set 
				h.QuantityLeft = (h.QuantityLeft - 1)
			from
				product.tVoucher h
			where
				h.VoucherCode = @VoucherCode
			
			
			select 
				vi.DiscountValue, 
				dt.DiscountTypeId,
				vi.VoucherInfoId
			from 
				product.tVoucher v 
				inner join product.tVoucherInfo vi
						on v.VoucherInfoId = vi.VoucherInfoId
				inner join product.tDiscountType dt
						on vi.DiscountTypeId = dt.DiscountTypeId
			where 
				v.VoucherCode = @VoucherCode 
									
		end
		-- if customer id is sent search after a specific code valid only to the id
		-- and see if it is still valid, Today CustomerId > can not occure
		
		--else if @CustomerId > 0
		--begin						
		--end
	
	insert into product.tVoucherLog(VoucherId, OldSystemDiscountCodeId, Used, OrderId)
	select
		(select VoucherId from product.tVoucher where VoucherCode = @VoucherCode),
		(select Id from product.tOldSystemVoucherCodes where DiscountId = @VoucherCode),
		GETDATE(),
		@orderId  -- (case when @orderId is null then 2 end)
		
	commit transaction

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
					
	end catch		 
end
GO
