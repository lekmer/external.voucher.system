SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherCheckOLD]
	@VoucherCode	nvarchar (50),
	@CustomerId 	int,
	@SiteApplicationName			nvarchar(40),
	@customers		xml = null
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		-- Scensum skickar med customer Id när man är inloggad och då funkar itne logiken
		-- då man kan ha en speciell kod länkad till ett customer Id. Så i detta
		-- fall säger at att customer Id alltid är noll till vi reder ut hur logiken ska vara.
		set @CustomerId = 0
		
		declare @Return int
		set @Return = 1
		declare @SiteId int
		set @SiteId = 0
		
		set @SiteId = (select siteId from product.tSite where SiteTitle = @SiteApplicationName)
			-- 1 är en aktiv engångsrabatt, 
			-- 2 är en använd engångsrabatt och 3 är en rabatt som aldrig går ur.
			
		if @SiteId != 0
		begin
		
			if @CustomerId = 0
			begin
			-- om customerId är 0 kolla om koden är länkad till en specifik Id
			-- för i sådant fall ska den inte gå att förbruka om man inte skickar med detta id
				if not exists (select 1
						from product.tVoucherCustomer vc
						inner join product.tVoucher v
							on vc.VoucherId = v.VoucherId				
						where v.VoucherCode = @VoucherCode)
				begin
						if exists (select 1 
							from product.tOldSystemVoucherCodes
							where
							(DiscountId = @VoucherCode AND [Status] = 1) --@VoucherCode
							OR 
							(DiscountId = @VoucherCode AND [Status] = 3))
							--lägger till att siteid måste vara lekmer.se
							AND 
							@SiteId = 1
						begin
							set @Return = 0		
						end
						-- if the code was not found in the old table
						-- search in the new system
						if @Return = 1
						begin
							if exists (select * 
									from product.tVoucher v
										inner join product.tVoucherInfo vi
											on v.VoucherInfoId = vi.VoucherInfoId
									where 
									VoucherCode = @VoucherCode
									AND (QuantityLeft > 0 ) 
									AND (
											(vi.ValidTo is null or GETDATE() < (vi.ValidTo)) 
													and 
											(v.ValidTo is null or GETDATE() < (v.ValidTo))
										)										
									AND (vi.IsActive = 1)
									AND (v.IsActive = 1 or v.IsActive is null)
									AND (vi.ValidFrom < GETDATE())
									AND 
									-- New change (09/27) - Allows the use of codes in multiple channels
									-- Checks to see if the site id is in the current ChannelGroup
									(@SiteId in 
											(select SiteId
											from product.tChannelGroup
											where ChannelGroupId = vi.ChannelGroupId
											)))
							begin
								set @Return = 0		
							end
						end
				end
			end
		-- if customer id is sent search after a specific code valid only to the id
		-- and see if it is still valid
			else if @CustomerId > 0
			begin
				if exists (select 1 
					from 
						product.tVoucher v 
						inner join product.tVoucherInfo vi
								on v.VoucherInfoId = vi.VoucherInfoId
						inner join product.tVoucherCustomer vc
								on v.VoucherId = vc.VoucherId
						inner join product.tCustomer c
								on c.CustomerId = vc.CustomerId	
					where 
						vc.CustomerId = @CustomerId
						AND vc.VoucherId = (select VoucherId from product.tVoucher where VoucherCode = @VoucherCode)
						AND (v.QuantityLeft > 0 ) 
						AND (
										(vi.ValidTo is null or GETDATE() < (vi.ValidTo)) 
												and 
										(v.ValidTo is null or GETDATE() < (v.ValidTo))
							)	
						AND (vi.IsActive = 1)	AND (vi.ValidFrom < GETDATE())
						AND 
						-- New change (09/27) - Allows the use of codes in multiple channels
						-- Checks to see if the site id is in the current ChannelGroup
						(@SiteId in 
								(select SiteId
								from product.tChannelGroup
								where ChannelGroupId = vi.ChannelGroupId
								)))
					begin
						set @Return = 0
					end
			end
		
		end
	commit transaction
	return @Return
	end try
	begin catch
		set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		return @Return
	end catch		 
end
GO
