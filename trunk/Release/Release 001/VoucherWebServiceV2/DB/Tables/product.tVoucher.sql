CREATE TABLE [product].[tVoucher]
(
[VoucherId] [int] NOT NULL IDENTITY(1, 1),
[VoucherInfoId] [int] NOT NULL,
[VoucherCode] [nvarchar] (100) COLLATE Finnish_Swedish_CS_AS NOT NULL,
[QuantityLeft] [int] NOT NULL,
[ValidTo] [datetime] NULL,
[IsActive] [bit] NULL,
[Reserved] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVoucher] ADD CONSTRAINT [PK__tVoucher__3AEE79214E88ABD4] PRIMARY KEY CLUSTERED  ([VoucherId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tVoucher_1] ON [product].[tVoucher] ([VoucherCode]) INCLUDE ([IsActive], [QuantityLeft], [Reserved], [ValidTo]) ON [PRIMARY]
GO
ALTER TABLE [product].[tVoucher] ADD CONSTRAINT [FK__tVoucher__ValidT__5070F446] FOREIGN KEY ([VoucherInfoId]) REFERENCES [product].[tVoucherInfo] ([VoucherInfoId])
GO
ALTER TABLE [product].[tVoucher] ADD CONSTRAINT [fk_voucherInfoId] FOREIGN KEY ([VoucherInfoId]) REFERENCES [product].[tVoucherInfo] ([VoucherInfoId])
GO
