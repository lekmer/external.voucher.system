CREATE TABLE [user].[tVoucherCreationRestrictions]
(
[UserId] [nvarchar] (70) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[MaximumAmount] [int] NULL,
[MaximumPercentage] [int] NULL,
[MaximumQuantityBatch] [int] NULL,
[MaximumStartQuantityCode] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [user].[tVoucherCreationRestrictions] ADD CONSTRAINT [PK__tVoucher__1788CC4C07C12930] PRIMARY KEY CLUSTERED  ([UserId]) ON [PRIMARY]
GO
