CREATE TABLE [product].[tOldSystemVoucherCodes]
(
[Id] [int] NOT NULL,
[DiscountType] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[DiscountValue] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[DiscountTitle] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Status] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[DiscountId] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[DiscountPerArticle] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tOldSystemVoucherCodes] ADD CONSTRAINT [PK__tOldSyst__3214EC071FCDBCEB] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
