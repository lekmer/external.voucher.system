CREATE TABLE [product].[tDiscountType]
(
[DiscountTypeId] [int] NOT NULL IDENTITY(1, 1),
[DiscountType] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tDiscountType] ADD CONSTRAINT [PK__tDiscoun__6CCE1DB6534D60F1] PRIMARY KEY CLUSTERED  ([DiscountTypeId]) ON [PRIMARY]
GO
