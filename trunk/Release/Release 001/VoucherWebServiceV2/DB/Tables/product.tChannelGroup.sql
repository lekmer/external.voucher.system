CREATE TABLE [product].[tChannelGroup]
(
[ChannelGroupId] [int] NOT NULL,
[SiteId] [int] NOT NULL,
[Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tChannelGroup] ADD CONSTRAINT [PK__tChannel__F58D816B74AE54BC] PRIMARY KEY CLUSTERED  ([ChannelGroupId], [SiteId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tChannelGroup] ADD CONSTRAINT [fk_siteId] FOREIGN KEY ([SiteId]) REFERENCES [product].[tSite] ([SiteId])
GO
