CREATE TABLE [product].[tVoucherCreationLog]
(
[CreationId] [int] NOT NULL IDENTITY(1, 1),
[VoucherInfoId] [int] NULL,
[Date] [datetime] NOT NULL,
[CreatedBy] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [product].[tVoucherCreationLog] ADD CONSTRAINT [PK__tVoucher__03DEDB217F2BE32F] PRIMARY KEY CLUSTERED  ([CreationId]) ON [PRIMARY]
GO
