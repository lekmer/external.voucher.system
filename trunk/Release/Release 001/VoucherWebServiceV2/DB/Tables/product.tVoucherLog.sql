CREATE TABLE [product].[tVoucherLog]
(
[VoucherUsedId] [int] NOT NULL IDENTITY(1, 1),
[VoucherId] [int] NULL,
[OldSystemDiscountCodeId] [int] NULL,
[Used] [datetime] NOT NULL,
[OrderId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVoucherLog] ADD CONSTRAINT [PK__tVoucher__3FBEAD326D0D32F4] PRIMARY KEY CLUSTERED  ([VoucherUsedId]) ON [PRIMARY]
GO
