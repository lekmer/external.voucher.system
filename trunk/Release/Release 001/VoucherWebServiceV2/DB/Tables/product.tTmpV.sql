CREATE TABLE [product].[tTmpV]
(
[VoucherId] [int] NOT NULL,
[AffiliateCode] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tTmpV] ADD CONSTRAINT [PK__tTmpV__3AEE79210C85DE4D] PRIMARY KEY CLUSTERED  ([VoucherId]) ON [PRIMARY]
GO
