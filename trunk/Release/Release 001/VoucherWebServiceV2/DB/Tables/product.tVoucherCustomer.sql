CREATE TABLE [product].[tVoucherCustomer]
(
[CustomerId] [int] NOT NULL,
[VoucherId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVoucherCustomer] ADD CONSTRAINT [PK__tVoucher__A700834A5EBF139D] PRIMARY KEY CLUSTERED  ([CustomerId], [VoucherId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tVoucherCustomer] ADD CONSTRAINT [FK__tVoucherC__Custo__619B8048] FOREIGN KEY ([CustomerId]) REFERENCES [product].[tCustomer] ([CustomerId])
GO
ALTER TABLE [product].[tVoucherCustomer] ADD CONSTRAINT [FK__tVoucherC__Vouch__60A75C0F] FOREIGN KEY ([VoucherId]) REFERENCES [product].[tVoucher] ([VoucherId])
GO
