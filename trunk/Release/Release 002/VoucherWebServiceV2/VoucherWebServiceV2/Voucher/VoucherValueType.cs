﻿using System;

namespace Voucher
{
	[Serializable]
	public enum VoucherValueType
	{
		Unknown = 0,
		Percentage = 1,
		Price = 2,
		GiftCard = 3
	}
}