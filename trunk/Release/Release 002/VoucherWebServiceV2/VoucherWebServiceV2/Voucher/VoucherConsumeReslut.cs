﻿using System;

namespace Voucher
{
	[Serializable]
	public class VoucherConsumeReslut
	{
		public VoucherValueType ValueType { get; set; }
		public bool ValidCode { get; set; }
		public decimal DiscountValue { get; set; }
		public int VoucherbatchId { get; set; }
	}

	[Serializable]
	public class VoucherConsumeReslutNew
	{
		public VoucherValueType ValueType { get; set; }
		public bool IsValid { get; set; }
		public decimal DiscountValue { get; set; }
		public int VoucherBatchId { get; set; }
		public decimal AmountLeft { get; set; }
	}
}