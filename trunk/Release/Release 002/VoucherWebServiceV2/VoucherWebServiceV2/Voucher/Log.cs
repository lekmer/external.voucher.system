﻿using System;
using System.Configuration;
using System.IO;

namespace Voucher
{
	internal class Log
	{
		private static readonly string LOG_NAME = "Log";

		public void LogErrorToFile(Exception e)
		{
			var sw = File.AppendText(ConfigurationManager.AppSettings[LOG_NAME] + "." + GetSafeDateString());
			try
			{
				sw.Write("[" + DateTime.Now + "]    ");
				sw.WriteLine("Process failed:   " + e.Message);
				sw.WriteLine("Stacktrace:   " + e.StackTrace);
			}
			finally
			{
				sw.Close();
			}
		}

		public void LogApplicationStart()
		{
			var sw = File.AppendText(ConfigurationManager.AppSettings[LOG_NAME] + "." + GetSafeDateString());
			try
			{
				sw.WriteLine("##START##[" + DateTime.Now + "] ::: Voucher Application Started");
				sw.Close();
			}
			catch (Exception e)
			{
				sw.Close();
				LogErrorToFile(e);
			}
		}

		private static string GetSafeDateString()
		{
			return DateTime.Today.ToString("yyyy-MM-dd");
		}

		public void LogProcessDurationTime(TimeSpan elapsed)
		{
			var sw = File.AppendText(ConfigurationManager.AppSettings[LOG_NAME] + "." + GetSafeDateString());
			try
			{
				sw.WriteLine("[Time Elapsed]: {0} ms", elapsed);
				sw.Close();
			}
			catch (Exception e)
			{
				sw.Close();
				LogErrorToFile(e);
			}
		}

		public void Debug(string txt)
		{
			var sw = File.AppendText(ConfigurationManager.AppSettings[LOG_NAME] + "." + GetSafeDateString());
			try
			{
				sw.WriteLine(txt);
				sw.Close();
			}
			catch (Exception e)
			{
				sw.Close();
				LogErrorToFile(e);
			}
		}
	}
}