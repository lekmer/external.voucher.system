﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Voucher
{
	public class VoucherService
	{
		private static readonly string SqlDbConn = ConfigurationManager.ConnectionStrings["Online"].ToString();

		public VoucherConsumeReslut VoucherCheck(string code, string siteApplicationName)
		{
			var voucherConsumeReslut = new VoucherConsumeReslut();

			using (var connection = new SqlConnection(SqlDbConn))
			{
				var cmd = new SqlCommand("backoffice.pVoucherCheck", connection)
				{
					CommandType = CommandType.StoredProcedure,
					CommandText = "backoffice.pVoucherCheck",
					CommandTimeout = 180
				};

				cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;
				cmd.Parameters.Add("@SiteApplicationName", SqlDbType.NVarChar, 40).Value = siteApplicationName;

				try
				{
					connection.Open();
					SqlDataReader reader = cmd.ExecuteReader();

					if (reader.Read())
					{
						voucherConsumeReslut.DiscountValue = (decimal)reader.GetValue(0);
						voucherConsumeReslut.ValueType = (VoucherValueType)(reader.GetInt32(1));
						voucherConsumeReslut.VoucherbatchId = reader.GetInt32(2);
						voucherConsumeReslut.ValidCode = true;
					}

					return voucherConsumeReslut;
				}
				catch (Exception e)
				{
					new Log().LogErrorToFile(e);
				}
			}

			return voucherConsumeReslut;
		}
		
		public VoucherConsumeReslutNew VoucherCheckNew(string code, string siteApplicationName)
		{
			var voucherConsumeReslut = new VoucherConsumeReslutNew();

			using (var connection = new SqlConnection(SqlDbConn))
			{
				var cmd = new SqlCommand("backoffice.pVoucherCheck", connection)
				{
					CommandType = CommandType.StoredProcedure,
					CommandText = "backoffice.pVoucherCheck",
					CommandTimeout = 180
				};

				cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;
				cmd.Parameters.Add("@SiteApplicationName", SqlDbType.NVarChar, 40).Value = siteApplicationName;

				try
				{
					connection.Open();
					SqlDataReader reader = cmd.ExecuteReader();

					if (reader.Read())
					{
						voucherConsumeReslut.DiscountValue = (decimal)reader.GetValue(0);
						voucherConsumeReslut.ValueType = (VoucherValueType)(reader.GetInt32(1));
						voucherConsumeReslut.VoucherBatchId = reader.GetInt32(2);
						voucherConsumeReslut.AmountLeft = reader.GetValue(3) is DBNull ? 0.0m : (decimal)reader.GetValue(3);
						voucherConsumeReslut.IsValid = true;
					}

					return voucherConsumeReslut;
				}
				catch (Exception e)
				{
					new Log().LogErrorToFile(e);
				}
			}

			return voucherConsumeReslut;
		}

		public VoucherConsumeReslut VoucherConsume(string code, int orderId, decimal usedAmount, string siteApplicationName)
		{
			var voucherConsumeReslut = new VoucherConsumeReslut();

			int oid = 0;
			if (orderId > 0)
			{
				oid = orderId;
			}

			if (VoucherCheck(code, siteApplicationName).ValidCode)
			{
				using (var connection = new SqlConnection(SqlDbConn))
				{
					var cmd = new SqlCommand("backoffice.[pVoucherConsume]", connection)
					{
						CommandType = CommandType.StoredProcedure,
						CommandText = "backoffice.[pVoucherConsume]",
						CommandTimeout = 180
					};

					cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;
					cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = oid;
					cmd.Parameters.Add("@UsedAmount", SqlDbType.Decimal).Value = usedAmount;

					try
					{
						connection.Open();
						SqlDataReader reader = cmd.ExecuteReader();

						if (reader.Read())
						{
							voucherConsumeReslut.DiscountValue = (decimal)reader.GetValue(0);
							voucherConsumeReslut.ValueType = (VoucherValueType)(reader.GetInt32(1));
							voucherConsumeReslut.VoucherbatchId = reader.GetInt32(2);
							voucherConsumeReslut.ValidCode = true;
						}

						return voucherConsumeReslut;
					}
					catch (Exception e)
					{
						new Log().LogErrorToFile(e);
					}
				}
			}
			return voucherConsumeReslut;
		}

		public VoucherConsumeReslutNew VoucherConsumeNew(string code, int orderId, decimal usedAmount, string siteApplicationName)
		{
			var voucherConsumeReslut = new VoucherConsumeReslutNew();

			int oid = 0;
			if (orderId > 0)
			{
				oid = orderId;
			}

			if (VoucherCheck(code, siteApplicationName).ValidCode)
			{
				using (var connection = new SqlConnection(SqlDbConn))
				{
					var cmd = new SqlCommand("backoffice.[pVoucherConsume]", connection)
					{
						CommandType = CommandType.StoredProcedure,
						CommandText = "backoffice.[pVoucherConsume]",
						CommandTimeout = 180
					};

					cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;
					cmd.Parameters.Add("@OrderId", SqlDbType.Int).Value = oid;
					cmd.Parameters.Add("@UsedAmount", SqlDbType.Decimal).Value = usedAmount;

					try
					{
						connection.Open();
						SqlDataReader reader = cmd.ExecuteReader();

						if (reader.Read())
						{
							voucherConsumeReslut.DiscountValue = (decimal)reader.GetValue(0);
							voucherConsumeReslut.ValueType = (VoucherValueType)(reader.GetInt32(1));
							voucherConsumeReslut.VoucherBatchId = reader.GetInt32(2);
							voucherConsumeReslut.AmountLeft = reader.GetValue(3) is DBNull ? 0.0m : (decimal)reader.GetValue(3);
							voucherConsumeReslut.IsValid = true;
						}

						return voucherConsumeReslut;
					}
					catch (Exception e)
					{
						new Log().LogErrorToFile(e);
					}
				}
			}
			return voucherConsumeReslut;
		}

		public bool VoucherReserve(string code)
		{
			using (var connection = new SqlConnection(SqlDbConn))
			{
				var cmd = new SqlCommand("backoffice.pVoucherReserve", connection)
				{
					CommandType = CommandType.StoredProcedure,
					CommandText = "backoffice.pVoucherReserve",
					CommandTimeout = 180
				};

				cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;

				try
				{
					connection.Open();
					return cmd.ExecuteNonQuery() > 0;
				}
				catch (Exception e)
				{
					new Log().LogErrorToFile(e);
				}
			}

			return false;
		}

		public bool VoucherRelease(string code)
		{
			using (var connection = new SqlConnection(SqlDbConn))
			{
				var cmd = new SqlCommand("backoffice.pVoucherRelease", connection)
				{
					CommandType = CommandType.StoredProcedure,
					CommandText = "backoffice.pVoucherRelease",
					CommandTimeout = 180
				};

				cmd.Parameters.Add("@VoucherCode", SqlDbType.NVarChar, 50).Value = code;

				try
				{
					connection.Open();
					return cmd.ExecuteNonQuery() > 0;
				}
				catch (Exception e)
				{
					new Log().LogErrorToFile(e);
				}

				return false;
			}
		}
	}
}