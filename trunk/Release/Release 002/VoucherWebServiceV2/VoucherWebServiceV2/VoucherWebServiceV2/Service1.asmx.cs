﻿using System;
using System.Web.Services;
using Voucher;

namespace VoucherWebServiceV2
{
	/// <summary>
	/// Summary description for Service1
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class Service1 : WebService
	{
		private readonly static VoucherService _voucherService = new VoucherService();

		#region Obsolete methods, for backward compability

		[WebMethod(Description = "Validates a Voucher and returns a VoucherResult object")]
		public VoucherConsumeReslut VoucherCheck(int customerId, string code, string siteId)
		{
			if (code == null) throw new ArgumentNullException("code");
			if (siteId == null) throw new ArgumentNullException("siteId");

			return _voucherService.VoucherCheck(code, siteId);
		}

		[WebMethod(Description = "Consumes a Voucher and returns a VoucherResult object")]
		public VoucherConsumeReslut VoucherConsume(int customerId, string code, int orderId, string siteId)
		{
			if (code == null) throw new ArgumentNullException("code");
			if (siteId == null) throw new ArgumentNullException("siteId");

			// Used amount = 0
			return _voucherService.VoucherConsume(code, orderId, 0.0m, siteId);
		}

		#endregion

		[WebMethod(Description = "Validates a Voucher and returns a VoucherResult object")]
		public VoucherConsumeReslutNew VoucherCheckNew(string code, string siteApplicationName)
		{
			if (code == null) throw new ArgumentNullException("code");
			if (siteApplicationName == null) throw new ArgumentNullException("siteApplicationName");

			return _voucherService.VoucherCheckNew(code, siteApplicationName);
		}

		[WebMethod(Description = "Consumes a Voucher and returns a VoucherResult object")]
		public VoucherConsumeReslutNew VoucherConsumeNew(string code, int orderId, decimal usedAmount, string siteApplicationName)
		{
			if (code == null) throw new ArgumentNullException("code");
			if (siteApplicationName == null) throw new ArgumentNullException("siteApplicationName");
			if (usedAmount < 0) throw new ArgumentOutOfRangeException("usedAmount");

			return _voucherService.VoucherConsumeNew(code, orderId, usedAmount, siteApplicationName);
		}

		[WebMethod(Description = "Reserves a Voucher")]
		public bool VoucherReserve(string code)
		{
			if (code == null) throw new ArgumentNullException("code");

			return _voucherService.VoucherReserve(code);
		}

		[WebMethod(Description = "Releases a reserved Voucher")]
		public bool VoucherRelease(string code)
		{
			if (code == null) throw new ArgumentNullException("code");

			return _voucherService.VoucherRelease(code);
		}
	}
}