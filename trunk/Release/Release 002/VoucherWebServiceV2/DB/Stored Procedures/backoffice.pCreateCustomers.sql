SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pCreateCustomers]
	@CustomerXml 	xml
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
-- @CustomerXml example:<customers>
--							<customer id="1"
--									  value="email@email.com"	-- Optional
--									  type="12345ABCDE" />		-- Optional
--							<customer id="2" />
--						</customers>

	set nocount on
	begin try
		begin transaction
		
		declare @Return int
		set @Return = 1
		
		declare @tCustomer table
		(
			CustomerId int not null,
			Value nvarchar(320) null,
			[Type] nvarchar(250) null
		)
		
		insert @tCustomer(CustomerId, Value, [Type])
		select
			T.c.value('@id', 'int'),
			T.c.value('@value', 'nvarchar(320)'),
			T.c.value('@type', 'nvarchar(250)')
		from @CustomerXml.nodes('/customers/customer') T(c)
		
		if @Return = 1
		begin
			-- INTE TESTAD
			insert into product.tCustomer(CustomerId, Value, [Type])
			select
				CustomerId,
				Value,
				[Type]
			from @tCustomer tc
			where tc.CustomerId not in (Select 1 from product.tCustomer)	
			
			set @Return = 0		
			
		
		end

	
	commit transaction
	return @Return
	end try
	begin catch
		set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		return @Return
	end catch		 
end
GO
