SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherReserve]
	@VoucherCode	nvarchar (50)
	
AS
begin
	--set nocount on
	begin try
	
	begin transaction
			
			-- Update only if it is a onetime code
			update
				product.tVoucher
			set
				Reserved = 1
			where
				VoucherCode = @VoucherCode
				and QuantityLeft <= 1
				and Reserved = 0
			
	commit	
	end try
	begin catch
		
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
	
		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
		
	end catch		 
end
GO
