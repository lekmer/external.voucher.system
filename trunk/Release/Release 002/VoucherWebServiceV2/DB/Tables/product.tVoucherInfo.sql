CREATE TABLE [product].[tVoucherInfo]
(
[VoucherInfoId] [int] NOT NULL IDENTITY(1, 1),
[ValidFrom] [datetime] NOT NULL,
[ValidTo] [datetime] NULL,
[Prefix] [varchar] (32) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Suffix] [varchar] (32) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Quantity] [int] NOT NULL,
[DiscountTypeId] [int] NOT NULL,
[DiscountValue] [decimal] (18, 0) NOT NULL,
[IsActive] [bit] NOT NULL,
[ChannelGroupId] [int] NOT NULL,
[DiscountTitle] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NbrOfToken] [int] NOT NULL,
[OriginalStartQuantityPerCode] [int] NOT NULL,
[ValidInDays] [int] NULL,
[CreatedBy] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [product].[tVoucherInfo] ADD
CONSTRAINT [FK_tVoucherInfo_tDiscountType] FOREIGN KEY ([DiscountTypeId]) REFERENCES [product].[tDiscountType] ([DiscountTypeId])
ALTER TABLE [product].[tVoucherInfo] ADD 
CONSTRAINT [PK_tVoucherInfo] PRIMARY KEY CLUSTERED  ([VoucherInfoId]) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE trigger [product].[inserttVoucherInfo]
on [product].[tVoucherInfo]
for insert--, update
as
if exists (select *
	from INSERTED i
	where i.discountTypeId = 2
	and i.ChannelGroupId > 10) 
  begin
    RAISERROR ('Error, you can not use discountType 2 on more than 1 Channel.', 
               16, -- Severity.
               1 -- State.
               );
	rollback
	--return
  end  
else
  print 'Trigger: Allowed! Batch created.'
GO
