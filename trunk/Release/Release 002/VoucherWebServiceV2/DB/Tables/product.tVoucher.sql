CREATE TABLE [product].[tVoucher]
(
[VoucherId] [int] NOT NULL IDENTITY(1, 1),
[VoucherInfoId] [int] NOT NULL,
[VoucherCode] [nvarchar] (100) COLLATE Finnish_Swedish_CS_AS NOT NULL,
[QuantityLeft] [int] NOT NULL,
[ValidTo] [datetime] NULL,
[IsActive] [bit] NULL,
[Reserved] [bit] NULL,
[AmountLeft] [decimal] (18, 0) NULL
) ON [PRIMARY]
ALTER TABLE [product].[tVoucher] ADD
CONSTRAINT [FK_tVoucher_tVoucherInfo] FOREIGN KEY ([VoucherInfoId]) REFERENCES [product].[tVoucherInfo] ([VoucherInfoId])
ALTER TABLE [product].[tVoucher] ADD 
CONSTRAINT [PK_tVoucher] PRIMARY KEY CLUSTERED  ([VoucherId]) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tVoucher_VoucherCode] ON [product].[tVoucher] ([VoucherCode]) INCLUDE ([IsActive], [QuantityLeft], [Reserved], [ValidTo]) ON [PRIMARY]

GO
