CREATE TABLE [product].[tTmpV]
(
[VoucherId] [int] NOT NULL,
[AffiliateCode] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [product].[tTmpV] ADD 
CONSTRAINT [PK_tTmpV] PRIMARY KEY CLUSTERED  ([VoucherId]) ON [PRIMARY]
GO
