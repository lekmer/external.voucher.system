CREATE TABLE [product].[tVoucherCreationLog]
(
[CreationId] [int] NOT NULL IDENTITY(1, 1),
[VoucherInfoId] [int] NULL,
[Date] [datetime] NOT NULL,
[CreatedBy] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [product].[tVoucherCreationLog] ADD 
CONSTRAINT [PK_tVoucherCreationLog] PRIMARY KEY CLUSTERED  ([CreationId]) ON [PRIMARY]
GO
