SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pReportManagerLekmerFinanceVoucherValue]
	@FromDate		datetime,
	@ToDate			datetime,
	@ChannelGroupId	int,
	--@OfferId
	@DiscountTypeId	int,
	@ActiveCodes	int
	
AS
begin
	set nocount on
		
	begin try
		--set @ValidFrom = coalesce(@ValidFrom, '2009-08-12 00:00:00.000')
		--set @ValidTo = coalesce(@ValidTo, '2099-08-12 00:00:00.000')
		
		select 
			vi.VoucherInfoId,
			vi.ValidFrom,
			--coalesce(vi.ValidTo, (select top 1 ValidTo from product.tVoucher
									--where VoucherInfoId = vi.VoucherInfoId)),
			vi.ValidTo,
			Quantity,
			DiscountTypeId,
			DiscountValue,
			IsActive,
			ChannelGroupId,
			DiscountTitle,
			[Description],
			OriginalStartQuantityPerCode,
			CreatedBy
		from 
			product.tVoucherInfo vi
			--left join product.tVoucher v
				--on vi.VoucherInfoId = v.VoucherInfoId
		where
		-- hämta aktiva koder
			vi.ChannelGroupId = @ChannelGroupId
			and
			vi.ValidFrom > @FromDate
			and
			vi.ValidTo < @ToDate

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		--print ERROR_MESSAGE() + ' ' + GETDATE() + ' ' + ERROR_PROCEDURE()
	end catch		 
end
GO
