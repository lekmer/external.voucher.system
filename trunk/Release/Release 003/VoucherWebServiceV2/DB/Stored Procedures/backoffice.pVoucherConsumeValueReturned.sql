
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherConsumeValueReturned]
	@VoucherCode	nvarchar (50)
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		--declare @Return int
		--set @Return = 1			
		
		--Set @Return = 
		create table #tTemp
		(
			DiscountValue	decimal,
			DiscountTypeId	int
		)
		insert into #tTemp
		select vi.DiscountValue, dt.DiscountTypeId
		from product.tVoucher v 
				inner join
					product.tVoucherInfo vi
						on v.VoucherInfoId = vi.VoucherInfoId
				inner join product.tDiscountType dt
						on vi.DiscountTypeId = dt.DiscountTypeId
		where v.VoucherCode = @VoucherCode 
		if exists (select 1 from #tTemp)
		begin
			select DiscountValue, DiscountTypeId
			from #tTemp
		end
		--set @Return = @VoucherCode
	commit transaction
	--return @Return
	end try
	begin catch
		--set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		--return @Return
	end catch		 
end
GO
