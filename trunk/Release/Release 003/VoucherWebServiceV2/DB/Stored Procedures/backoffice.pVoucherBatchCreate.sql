SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [backoffice].[pVoucherBatchCreate]
	@ValidFrom		DATETIME,
	@Prefix			VARCHAR (32),
	@Suffix			VARCHAR (32),
	@Quantity		INT,
	@DiscountValue	DECIMAL,
	@AppName		NVARCHAR(250),
	@NbrOfToken		INT,
	@CreatedBy		NVARCHAR(250)
AS
BEGIN
	DECLARE @DiscountTypeId	INT
	SET @DiscountTypeId = (SELECT DiscountTypeId FROM [product].[tDiscountType] WHERE DiscountType = 'GiftCard')
	
	DECLARE @ChannelGroupId	INT
	SET @ChannelGroupId = ( SELECT TOP(1) chg.ChannelGroupId
							FROM [product].[tChannelGroup] chg
							INNER JOIN [product].[tSite] s ON s.[SiteId] = chg.[SiteId]
							WHERE s.[SiteTitle] = @AppName)

	INSERT INTO [product].[tVoucherInfo] (
		ValidFrom,
		ValidTo,
		Prefix,
		Suffix,
		Quantity,
		DiscountTypeId,
		DiscountValue,
		IsActive,
		ChannelGroupId,
		DiscountTitle,
		[Description],
		NbrOfToken,
		OriginalStartQuantityPerCode,
		ValidInDays,
		CreatedBy
	)
	SELECT
		(CAST(@ValidFrom AS DATETIME)),
		NULL,
		@Prefix,
		@Suffix,
		@Quantity,
		@DiscountTypeId,
		@DiscountValue,
		1,
		@ChannelGroupId,
		'GiftCardViaEmail',
		'Gift card via email batch',
		@NbrOfToken,
		1,
		NULL,
		@CreatedBy
		
	SELECT @@IDENTITY
END
GO
