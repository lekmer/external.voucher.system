CREATE TABLE [product].[tChannelGroup]
(
[ChannelGroupId] [int] NOT NULL,
[SiteId] [int] NOT NULL,
[Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [product].[tChannelGroup] ADD
CONSTRAINT [FK_tChannelGroup_tSite] FOREIGN KEY ([SiteId]) REFERENCES [product].[tSite] ([SiteId])
ALTER TABLE [product].[tChannelGroup] ADD 
CONSTRAINT [PK_tChannelGroup] PRIMARY KEY CLUSTERED  ([ChannelGroupId], [SiteId]) ON [PRIMARY]
GO
