CREATE TABLE [archive].[tVoucher]
(
[VoucherId] [int] NOT NULL,
[VoucherInfoId] [int] NOT NULL,
[VoucherCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[QuantityLeft] [int] NOT NULL,
[ValidTo] [datetime] NULL,
[IsActive] [bit] NULL,
[Reserved] [bit] NULL,
[AmountLeft] [decimal] (18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [archive].[tVoucher] ADD CONSTRAINT [PK_tVoucher] PRIMARY KEY CLUSTERED  ([VoucherId]) ON [PRIMARY]
GO
ALTER TABLE [archive].[tVoucher] ADD CONSTRAINT [FK_tVoucher_tVoucherInfo] FOREIGN KEY ([VoucherInfoId]) REFERENCES [archive].[tVoucherInfo] ([VoucherInfoId])
GO
