SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-----------------------------------------------------------------------------
-- performance.pTableIndexCheckAndRebuild
--
-- Summary.....: A simpler variant of the scheduled rebuild indexes that takes 
--               all sorts of considerations.  
--      [generic].[pTableIndexCheckAndRebuild]
--         'Voucher',
--         'SAMPLED',
--         '<tables>
--			  <table tablename="product.tVoucher" fragmentationlevel="10"/>
--			  <table tablename="product.tVoucherInfo" fragmentationlevel="10"/>
--         </tables>',
--         1
-- Returns.....: -
-- Parameters..:
-- 
-- $Revision: 8 $
-- 
-- $History: pTableIndexCheckAndRebuild.sql $
-- 
-----------------------------------------------------------------------------
CREATE procedure [generic].[pTableIndexCheckAndRebuild]
(
   @DB nvarchar(50),
   @Mode nvarchar(50) = 'SAMPLED', --LIMITED, SAMPLED, or DETAILED,
   @Tables xml, 
   @Execute bit = 1,
   @OnlyOnlineRebuild bit = null 
)
as
begin
	set nocount on
-- exec [integration].[pCheckDefragmentation]
   declare @dbid smallint
           ,@objectid int
           ,@TableName nvarchar(255)
           ,@FragmentationLevel int 
           ,@ExcludeIndex nvarchar(max)
           ,@IndexName nvarchar(255)
           ,@HasVarcharMax bit
           ,@ActualFragmentationLevel decimal(10,2)
           ,@TmpStr nvarchar(max)
           ,@ObjName varchar(250)
           ,@ErrMsg			nvarchar(2048) 
           ,@SP				nvarchar(256)
           ,@Severity		int 
           ,@State			int 
           ,@ErrorNumber	int
           ,@LockTimeOut   int
   declare @tmp table (TableName nvarchar(255) not null, FragmentationLevel int not null, ExcludeIndex nvarchar(max) null, LockTimeOut int null)
   set @ObjName = object_name(@@procid)
   
   insert into @tmp (TableName, FragmentationLevel, ExcludeIndex, LockTimeOut)
   select T.c.value('(@tablename)[1]', 'nvarchar(255)') as 'TableName'
         ,T.c.value('(@fragmentationlevel)[1]', 'int') as 'FragmentationLevel'
         ,T.c.value('(@excludeindex)[1]', 'nvarchar(max)') as 'ExcludeIndex'
         ,case when len(rtrim(T.c.value('(@locktimeout)[1]', 'nvarchar(255)'))) = 0 then null 
               else T.c.value('(@locktimeout)[1]', 'int')
          end as 'LockTimeout'
     from @Tables.nodes('tables/table') as T(c)

   set @dbid = db_id(@db)
   if @dbid is null
   begin
       print N'invalid database'
       return 1
   end

   declare lcur cursor local fast_forward for 
      select TableName, FragmentationLevel, ExcludeIndex, LockTimeOut
      from @Tmp
   open lcur
   while 1=1
   begin 

      fetch next from lcur into @TableName, @FragmentationLevel, @ExcludeIndex, @LockTimeOut

      if @@fetch_status = -1 break     -- eof
      if @@fetch_status = -2 continue  -- record were deleted

      set @objectid = object_id(@TableName)

      if @objectid is null
      begin
          set @TmpStr = 'Invalid object ' + @TableName
          print @TmpStr
          continue
      end
      else
      begin

         set @TmpStr = 'Checking fragmentation for ' + @TableName
         print @TmpStr

         declare lcurIndexCheckFrag cursor local fast_forward for 
                  select IndexName, HasVarcharMax, cast(avg_fragmentation_in_percent as decimal(10,2))
                    from (select o.name as tablename, ix.name as indexname, 
                                 case when (select count(*) from sys.columns where object_id = @objectid and max_length = -1) > 0 then 1 else 0 end as hasvarcharmax, 
              	                  i.*
                            from sys.dm_db_index_physical_stats(@dbid, @objectid, null, null , @mode) i
                                   inner join sys.objects o on i.object_id = o.object_id
                                   inner join sys.indexes ix on o.object_id = ix.object_id and i.index_id = ix.index_id
                           where avg_fragmentation_in_percent > @FragmentationLevel
                             and (@ExcludeIndex is null or ix.name not in (select sepstring from [generic].[fStringToStringTable](@ExcludeIndex,';')))
                         ) t
         open lcurIndexCheckFrag
         while 1=1
         begin 
            declare @sql nvarchar(max)
            fetch next from lcurIndexCheckFrag into @IndexName, @HasVarcharMax, @ActualFragmentationLevel

            if @@fetch_status = -1 break     -- eof
            if @@fetch_status = -2 continue  -- record were deleted
            
            
            
            --if @HasVarcharMax = 0 /* = "Enterprise"
            --   set @sql = 'ALTER INDEX ' + @IndexName + ' ON ' + @TableName + ' REBUILD WITH (ONLINE = ON)'
            --else 
            set @sql = 'ALTER INDEX ' + @IndexName + ' ON ' + @TableName + ' REBUILD'   

            if @Execute = 1
            begin
               if @HasVarcharMax = 1 
                  and @OnlyOnlineRebuild = 1 
               begin
                  set @TmpStr = @TableName + ' had fragmentation ' + cast(@ActualFragmentationLevel as varchar(30)) + '. Ignoring rebuild since it has varcharmax, sql : ' + @sql
                  print @TmpStr
               end   
               else
               begin
                  set @TmpStr = @TableName + ' had fragmentation ' + cast(@ActualFragmentationLevel as varchar(30)) + '. Running : ' + @sql
                  print @TmpStr
                  begin try
                     if @LockTimeOut is null
                     begin
                        exec(@sql)
                     end
                     else
                     begin 
                        -- Add lock timeout to the rebuild command before we run it
                        set @sql = 'set lock_timeout ' + cast(@LockTimeOut as varchar(30)) + char(13) + @sql + char(13)
                        exec(@sql)
                     end
                  end try
                  begin catch
                     select 
                        @ErrMsg = error_message(), 
                        @SP = error_procedure(), 
                        @Severity = error_severity(), 
                        @State = error_state(), 
                        @ErrorNumber = error_number()
                        begin try
                           if error_number() = 1222  
                              set @ErrMsg = 'Lock Timeout occurred on table ' + @TableName + ' index ' + @IndexName + ' when trying to rebuild index. Index rebuild is ignored. (Error: ' + error_message() + ')'
                           print	@ErrMsg 	
                        end try
                        begin catch
                        end catch
                  end catch
               end
            end
            else 
            begin
               set @TmpStr = @TableName + ' had fragmentation ' + cast(@ActualFragmentationLevel as varchar(30)) + '. Rebuild sql : ' + @sql
               print @TmpStr
            end

         end
         if (cursor_status ('local', 'lcurIndexCheckFrag') in (0, 1))
         begin
            close lcurIndexCheckFrag
            deallocate lcurIndexCheckFrag
         end
      end
            
   end
   if (cursor_status ('local', 'lcur') in (0, 1))
   begin
      close lcur
      deallocate lcur
   end

   set @TmpStr = 'Done'
   print @TmpStr

end

GO
