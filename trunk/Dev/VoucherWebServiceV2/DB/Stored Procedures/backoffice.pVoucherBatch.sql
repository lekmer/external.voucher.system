
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherBatch]
	@ValidFrom		datetime,
	@ValidTo		datetime,
	@Prefix			varchar (32),
	@Suffix			varchar (32),
	@Quantity		int,
	@DiscountTypeId	int,
	@DiscountValue	decimal,
	@IsActive		bit,
	@ChannelGroupId	int,
	@DiscountTitle	nvarchar (250),
	@Description	nvarchar (250),
	@NbrOfToken		int,
	@OriginalStartQuantityPerCode	int,
	@ValidInDays	int,
	@CreatedBy		nvarchar(250) = NULL,
	@SpecialOffer	BIT = NULL
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		-- Voucher 'GiftCard'
		IF (@DiscountTypeId = 3 AND @OriginalStartQuantityPerCode > 1)
		BEGIN
			RAISERROR ('The voucher info with discount type = "GiftCard" should has the @OriginalStartQuantityPerCode = 1.', 16, 1)
		END
		
		declare @Return int
		set @Return = 1
		
		insert into product.tVoucherInfo(
			ValidFrom,
			ValidTo,
			Prefix,
			Suffix,
			Quantity,
			DiscountTypeId,
			DiscountValue,
			IsActive,
			ChannelGroupId,
			DiscountTitle,
			[Description],
			NbrOfToken,
			OriginalStartQuantityPerCode,
			ValidInDays,
			CreatedBy,
			[SpecialOffer])
		select
			(CAST(@ValidFrom AS datetime)),
			(CAST(@ValidTo AS datetime)),
			@Prefix,
			@Suffix,
			@Quantity,
			@DiscountTypeId,
			@DiscountValue,
			@IsActive,
			@ChannelGroupId,
			@DiscountTitle,
			@Description,
			@NbrOfToken,
			@OriginalStartQuantityPerCode,
			@ValidInDays,
			@CreatedBy,
			@SpecialOffer
		
		set @Return = 0

	commit transaction
	return @Return
	end try
	begin catch
		set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', @ErrMsg, GETDATE(), @SP)
		
		RETURN @Return
	END CATCH		 
END
GO
