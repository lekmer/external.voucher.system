SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [backoffice].[pVoucherBatchGiftCardGetByChannelCampaignDiscountValue]
	@DiscountValue	DECIMAL(18,0),
	@AppName		NVARCHAR(250),
	@CreatedBy		NVARCHAR(250)
AS
BEGIN
	SELECT 
		vi.[VoucherInfoId],
		vi.[Quantity]
	FROM
		[product].[tVoucherInfo] vi
		INNER JOIN [product].[tChannelGroup] chg ON chg.[ChannelGroupId] = vi.[ChannelGroupId]
		INNER JOIN [product].[tSite] s ON s.[SiteId] = chg.[SiteId]
	WHERE
		vi.[DiscountTypeId] = (SELECT DiscountTypeId FROM [product].[tDiscountType] WHERE DiscountType = 'GiftCard')
		AND vi.[DiscountValue] = @DiscountValue
		AND vi.[IsActive] = 1
		AND vi.[CreatedBy] = @CreatedBy
		AND s.[SiteTitle] = @AppName
END
GO
