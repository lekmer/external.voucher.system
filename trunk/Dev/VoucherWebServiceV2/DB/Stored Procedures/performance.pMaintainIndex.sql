SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [performance].[pMaintainIndex]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @minFragmentation FLOAT = 5.0 /* in percent, will not defrag if fragmentation less than specified */
	DECLARE @rebuildThreshold FLOAT = 30.0  /* in percent, greater than @rebuildThreshold will result in rebuild instead of reorg */

	DECLARE @db_id SMALLINT;
	DECLARE	@objectid INT;
	DECLARE	@indexid INT;
	DECLARE	@partitioncount BIGINT;
	DECLARE	@schemaname NVARCHAR(130); 
	DECLARE	@objectname NVARCHAR(130); 
	DECLARE	@indexname NVARCHAR(130); 
	DECLARE @allowPageLocks INT;
	DECLARE	@partitionnum BIGINT;
	DECLARE	@partitions BIGINT;
	DECLARE	@frag FLOAT;
	DECLARE	@command NVARCHAR(4000); 


	SET @db_id = DB_ID();

	IF @db_id IS NULL
	BEGIN
		PRINT N'Invalid database'
		RETURN
	END

	-- Conditionally select tables and indexes from the sys.dm_db_index_physical_stats function 
	-- and convert object and index IDs to names.
	SELECT
		object_id AS objectid,
		index_id AS indexid,
		partition_number AS partitionnum,
		avg_fragmentation_in_percent AS frag
	INTO #work_to_do
	FROM
		sys.dm_db_index_physical_stats(@db_id, NULL, NULL, NULL, 'LIMITED')
	WHERE
		avg_fragmentation_in_percent >= @minFragmentation
		AND index_id > 0 -- ignore heaps

	-- Declare the cursor for the list of partitions to be processed.
	DECLARE partitions CURSOR FOR SELECT * FROM #work_to_do

	-- Open the cursor.
	OPEN partitions;

	-- Loop through the partitions.
	WHILE ( 1 = 1 )
		BEGIN
			FETCH NEXT
			   FROM partitions
			   INTO @objectid, @indexid, @partitionnum, @frag

			IF @@FETCH_STATUS < 0 BREAK

			SELECT
				@objectname = QUOTENAME(o.name),
				@schemaname = QUOTENAME(s.name)
			FROM
				sys.objects AS o
				JOIN sys.schemas AS s ON s.schema_id = o.schema_id
			WHERE
				o.object_id = @objectid

			SELECT
				@indexname = QUOTENAME(name),
				@allowPageLocks = allow_page_locks
			FROM sys.indexes
			WHERE
				object_id = @objectid
				AND index_id = @indexid

			SELECT @partitioncount = COUNT(*)
			FROM sys.partitions
			WHERE
				object_id = @objectid
				AND index_id = @indexid

			IF @frag < @rebuildThreshold AND @allowPageLocks = 1
			SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + N' REORGANIZE';

			IF @frag >= @rebuildThreshold OR @allowPageLocks = 0
			SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + N' REBUILD';

			IF @partitioncount > 1 
			SET @command = @command + N' PARTITION=' + CAST(@partitionnum AS NVARCHAR(10));
				
			EXEC (@command);
	        
			PRINT N'Executed: ' + @command;
		END;

	-- Close and deallocate the cursor.
	CLOSE partitions;
	DEALLOCATE partitions;

	-- Drop the temporary table.
	DROP TABLE #work_to_do;

END

GO
