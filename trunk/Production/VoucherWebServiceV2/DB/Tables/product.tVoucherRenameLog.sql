CREATE TABLE [product].[tVoucherRenameLog]
(
[VoucherInfoId] [int] NOT NULL,
[VoucherId] [int] NOT NULL,
[OldVoucherCode] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NewVoucherCode] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[RenameDate] [datetime] NULL,
[RenamedBy] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Status] [nvarchar] (20) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
