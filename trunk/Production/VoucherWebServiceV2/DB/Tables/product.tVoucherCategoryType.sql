CREATE TABLE [product].[tVoucherCategoryType]
(
[VoucherCategoryTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVoucherCategoryType] ADD CONSTRAINT [PK__tVoucher__DA4370900EA330E9] PRIMARY KEY CLUSTERED  ([VoucherCategoryTypeId]) ON [PRIMARY]
GO
