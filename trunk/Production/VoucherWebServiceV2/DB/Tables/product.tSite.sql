CREATE TABLE [product].[tSite]
(
[SiteId] [int] NOT NULL IDENTITY(1, 1),
[SiteTitle] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [product].[tSite] ADD 
CONSTRAINT [PK_tSite] PRIMARY KEY CLUSTERED  ([SiteId]) ON [PRIMARY]
GO
