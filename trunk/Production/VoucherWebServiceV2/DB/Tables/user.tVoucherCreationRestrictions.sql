CREATE TABLE [user].[tVoucherCreationRestrictions]
(
[UserId] [nvarchar] (70) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[MaximumAmount] [int] NULL,
[MaximumPercentage] [int] NULL,
[MaximumQuantityBatch] [int] NULL,
[MaximumStartQuantityCode] [int] NULL
) ON [PRIMARY]
ALTER TABLE [user].[tVoucherCreationRestrictions] ADD 
CONSTRAINT [PK_tVoucherCreationRestrictions] PRIMARY KEY CLUSTERED  ([UserId]) ON [PRIMARY]
GO
