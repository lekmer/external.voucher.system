SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pReportManagerProlongBatches]
	@BatchId INT,
	@NewExpirationDate DATETIME,
	@Username NVARCHAR(50),
	@Password VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @Hexbin VARBINARY(MAX)
	DECLARE @tmpPassword NVARCHAR(50)

	DECLARE @OldValidTo DATETIME
	
	IF NOT EXISTS(SELECT 1 FROM [product].[tVoucherInfo] vi WHERE vi.[VoucherInfoId] = @BatchId)
	BEGIN
		SELECT 'Prolong batch [' + CONVERT(VARCHAR, @BatchId) + '] has failed! Check your batch id (system can''t find batches with id you have specified).'
		RETURN;
	END
	
	SET @OldValidTo = (SELECT vi.[ValidTo] FROM [product].[tVoucherInfo] vi WHERE vi.[VoucherInfoId] = @BatchId)

	-- Authenticate user
	SET @tmpPassword = (SELECT [Password] FROM Lekmer.[security].tSystemUser WHERE Username = @Username)
	SET @Hexbin = (SELECT (HASHBYTES('SHA1', @Password)))

	IF @tmpPassword = (CONVERT(VARCHAR(MAX), @Hexbin, 2)) -- Password OK
		AND 
		(SELECT StatusId FROM Lekmer.[security].tSystemUser WHERE Username = @Username) = 0 -- Online status OK
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION

			UPDATE
				vi
			SET
				vi.[ValidTo] = @NewExpirationDate
			FROM
				[product].[tVoucherInfo] vi
			WHERE
				vi.[VoucherInfoId] = @BatchId
				AND
				vi.[ChannelGroupId] IN (1,2,3,4,13,16)

			INSERT INTO product.[tVoucherProlongLog]
			(
				[VoucherInfoId],
				[OldValidTo],
				[NewValidTo],
				[ChangeDate],
				[ChangeBy],
				[Status],
				[Message]
			)
			SELECT
				vi.[VoucherInfoId],
				@OldValidTo,
				@NewExpirationDate,
				GETDATE(),
				@Username,
				'SUCCESS',
				null
			FROM
				[product].[tVoucherInfo] vi
			WHERE
				vi.[VoucherInfoId] = @BatchId
			
			SELECT 'New expiration date for batch [' + CONVERT(VARCHAR, @BatchId) + '] has been set to [' + CONVERT(VARCHAR, @NewExpirationDate) + '] Successfully'

		COMMIT	
		END TRY
		BEGIN CATCH
			IF @@trancount > 0 ROLLBACK TRANSACTION
			
			DECLARE @Message NVARCHAR(MAX)
			SET @Message = ERROR_MESSAGE()
			
			INSERT INTO product.[tVoucherProlongLog]
			(
				[VoucherInfoId],
				[OldValidTo],
				[NewValidTo],
				[ChangeDate],
				[ChangeBy],
				[Status],
				[Message]
			)
			SELECT
				vi.[VoucherInfoId],
				@OldValidTo,
				@NewExpirationDate,
				GETDATE(),
				@Username,
				'FAILED',
				@Message
			FROM
				[product].[tVoucherInfo] vi
			WHERE
				vi.[VoucherInfoId] = @BatchId

			SELECT 'Prolong batch [' + CONVERT(VARCHAR, @BatchId) + '] has failed! ' + @Message

		END CATCH
	END
	ELSE
	BEGIN
		SELECT 'Prolong batch [' + CONVERT(VARCHAR, @BatchId) + '] has failed! Check your Batch and Username/Password.'
	END
END

GO
