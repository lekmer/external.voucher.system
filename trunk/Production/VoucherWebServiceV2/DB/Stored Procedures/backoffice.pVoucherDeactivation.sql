
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherDeactivation]
	@VoucherInfoId	int = NULL,
	@VoucherCode	nvarchar(50) = NULL
	
AS
begin
	set nocount on
	begin try
	begin transaction
		
		Declare @VoucherId int
		set @VoucherId = (Select VoucherId from product.tVoucher where VoucherCode = @VoucherCode)
		
		if (@VoucherInfoId is not null and @VoucherInfoId > 0)
			begin
		
				update
					product.tVoucherInfo
				set
					IsActive = 0
				where
					VoucherInfoId = @VoucherInfoId
					and IsActive = 1
				
				Select 'Batch [' + cast(@VoucherInfoId as nvarchar) + '] deactivated' as Result
			end
		else
			begin
				
				update
					product.tVoucher
				set 
					IsActive = 0
				where
					VoucherId = @VoucherId
					and (IsActive = 1 or IsActive is null)
				
				Select 'Voucher [' + cast(@VoucherId as nvarchar) + '] deactivated' as Result
			end
			
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
					
	end catch		 
end

GO
