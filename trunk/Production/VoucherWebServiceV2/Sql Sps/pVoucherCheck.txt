USE [Voucher]
GO
/****** Object:  StoredProcedure [backoffice].[pVoucherCheck]    Script Date: 07/12/2011 08:41:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [backoffice].[pVoucherCheck]
	@VoucherCode	nvarchar (50),
	@CustomerId 	int,
	@SiteApplicationName			nvarchar(40),
	@customers		xml = null
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		-- Scensum skickar med customer Id n�r man �r inloggad och d� funkar itne logiken
		-- d� man kan ha en speciell kod l�nkad till ett customer Id. S� i detta
		-- fall s�ger at att customer Id alltid �r noll till vi reder ut hur logiken ska vara.
		set @CustomerId = 0
		
		declare @Return int
		set @Return = 1
		declare @SiteId int
		set @SiteId = 0
		
		set @SiteId = (select siteId from product.tSite where SiteTitle = @SiteApplicationName)

		if @SiteId != 0
		begin
			-- Has be rewritten, do the same for "If CustomerId > 0" when that function gets implemented
			if @CustomerId = 0
			begin
			
				if exists (select * 
						from product.tVoucher v
							inner join product.tVoucherInfo vi
								on v.VoucherInfoId = vi.VoucherInfoId
						where 
						VoucherCode = @VoucherCode
						AND (QuantityLeft > 0 ) 
						AND (
								(vi.ValidTo is null or GETDATE() < (vi.ValidTo)) 
										AND 
								(v.ValidTo is null or GETDATE() < (v.ValidTo))
							)										
						AND (vi.IsActive = 1)
						AND (v.IsActive = 1 or v.IsActive is null)	
						AND (vi.ValidFrom < GETDATE())
						AND (v.Reserved = 0 or v.Reserved is null)
						AND
						-- New change (09/27) - Allows the use of codes in multiple channels
						-- Checks to see if the site id is in the current ChannelGroup
						(@SiteId in 
								(select SiteId
								from product.tChannelGroup
								where ChannelGroupId = vi.ChannelGroupId
								)))
				begin
					select 
						vi.DiscountValue, 
						dt.DiscountTypeId,
						vi.VoucherInfoId
					from 
						product.tVoucher v 
						inner join product.tVoucherInfo vi
							on v.VoucherInfoId = vi.VoucherInfoId
						inner join product.tDiscountType dt
								on vi.DiscountTypeId = dt.DiscountTypeId
					where 
						v.VoucherCode = @VoucherCode 
				end				
			end
		-- if customer id is sent search after a specific code valid only to the id
		-- and see if it is still valid, Today CustomerId > can not occure
			else if @CustomerId > 0
			begin
				if exists (select 1 
					from 
						product.tVoucher v 
						inner join product.tVoucherInfo vi
								on v.VoucherInfoId = vi.VoucherInfoId
						inner join product.tVoucherCustomer vc
								on v.VoucherId = vc.VoucherId
						inner join product.tCustomer c
								on c.CustomerId = vc.CustomerId	
					where 
						vc.CustomerId = @CustomerId
						AND vc.VoucherId = (select VoucherId from product.tVoucher where VoucherCode = @VoucherCode)
						AND (v.QuantityLeft > 0 ) 
						AND (
										(vi.ValidTo is null or GETDATE() < (vi.ValidTo)) 
												and 
										(v.ValidTo is null or GETDATE() < (v.ValidTo))
							)	
						AND (vi.IsActive = 1)	AND (vi.ValidFrom < GETDATE())
						AND 
						-- New change (09/27) - Allows the use of codes in multiple channels
						-- Checks to see if the site id is in the current ChannelGroup
						(@SiteId in 
								(select SiteId
								from product.tChannelGroup
								where ChannelGroupId = vi.ChannelGroupId
								)))
					begin
						set @Return = 0
					end
			end
		
		end
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
					
	end catch		 
end
