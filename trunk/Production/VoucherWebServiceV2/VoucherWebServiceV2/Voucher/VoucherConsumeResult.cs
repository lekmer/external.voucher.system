﻿using System;

namespace Voucher
{
	[Serializable]
	public class VoucherConsumeResult
	{
		public VoucherValueType ValueType { get; set; }
		public bool IsValid { get; set; }
		public decimal DiscountValue { get; set; }
		public int VoucherBatchId { get; set; }
		public decimal AmountLeft { get; set; }
		public bool SpecialOffer { get; set; }
	}
}