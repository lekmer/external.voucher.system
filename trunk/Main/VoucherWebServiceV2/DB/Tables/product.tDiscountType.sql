CREATE TABLE [product].[tDiscountType]
(
[DiscountTypeId] [int] NOT NULL IDENTITY(1, 1),
[DiscountType] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [product].[tDiscountType] ADD 
CONSTRAINT [PK_tDiscountType] PRIMARY KEY CLUSTERED  ([DiscountTypeId]) ON [PRIMARY]
GO
