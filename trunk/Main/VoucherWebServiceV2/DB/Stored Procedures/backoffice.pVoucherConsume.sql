
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [backoffice].[pVoucherConsume]
	@VoucherCode	NVARCHAR (50),
	@OrderId		INT,
	@UsedAmount		DECIMAL(18,0) = NULL
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

			-- Voucher 'GiftCard'
			IF EXISTS (SELECT 1
					   FROM product.tVoucher v
					   INNER JOIN product.tVoucherInfo vi on vi.VoucherInfoId = v.VoucherInfoId
					   WHERE v.VoucherCode = @VoucherCode
					   AND vi.DiscountTypeId = 3)
				BEGIN
					DECLARE @AmountLeft DECIMAL(18,0)
					SELECT @AmountLeft = v.AmountLeft - @UsedAmount FROM product.tVoucher v WHERE v.VoucherCode = @VoucherCode
										
					UPDATE 
						v 
					SET 
						v.AmountLeft = @AmountLeft
						,v.QuantityLeft = (CASE WHEN @AmountLeft <= 0 THEN v.QuantityLeft - 1 ELSE v.QuantityLeft END)
					FROM
						product.tVoucher v
					WHERE
						v.VoucherCode = @VoucherCode
				END
			-- Voucher other
			ELSE
				BEGIN
					UPDATE 
						v 
					SET 
						v.QuantityLeft = (v.QuantityLeft - 1)
					FROM
						product.tVoucher v
					WHERE
						v.VoucherCode = @VoucherCode
				END
			
			SELECT 
				vi.DiscountValue, 
				dt.DiscountTypeId,
				vi.VoucherInfoId,
				v.AmountLeft,
				vi.[SpecialOffer]
			FROM 
				product.tVoucher v 
				INNER JOIN product.tVoucherInfo vi ON v.VoucherInfoId = vi.VoucherInfoId
				INNER JOIN product.tDiscountType dt ON vi.DiscountTypeId = dt.DiscountTypeId
			WHERE 
				v.VoucherCode = @VoucherCode 
		
			INSERT INTO product.tVoucherLog(VoucherId, UsageDate, OrderId, AmountUsed)
			SELECT
				(SELECT VoucherId FROM product.tVoucher WHERE VoucherCode = @VoucherCode),
				GETDATE(),
				@OrderId,
				@UsedAmount
									
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		if @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
