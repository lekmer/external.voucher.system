
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [backoffice].[pVoucherGenerateCodes]
	@VoucherInfoId	int,
	@nbrOfVouchers	int,
	@customers		xml = null,
	@ValidTo		smalldatetime = null
as
begin
	set nocount on
	begin try
		begin transaction
		
		declare
		@nbrOfToken			int,
		@suffix				varchar(32),
		@prefix				varchar(32),
		@DiscountValue		decimal(18,0),
		@OriginalQuantity	int

		select
			@nbrOfToken = NbrOfToken,
			@suffix = Suffix,
			@prefix = Prefix,
			@DiscountValue = DiscountValue,
			@OriginalQuantity = OriginalStartQuantityPerCode
		from
			product.tVoucherInfo
		where
			VoucherInfoId = @VoucherInfoId

		set @nbrOfToken = @nbrOfToken - 2 -- reduce length by two characters since we add a 2 digit checksum.

		if @nbrOfVouchers is null and @customers is null
		begin
			raiserror ('Either @nbrOfVouchers or @customers must have a non null value.', 16, 1)
		end
		

		if @nbrOfVouchers is null -- Customer
		begin
			select
				@nbrOfVouchers = count(*)
			from
				@customers.nodes('customers/customer') as T(c)
		end


		declare @codes table (code varchar(32) COLLATE Finnish_Swedish_CI_AS)

		declare
			@voucherCount	int,
			@tokenCount		int,
			@validTokens	varchar(100),
			@lenValidTokens	int,
			@pos			int,
			@sum			int,
			@token			char,
			@code			varchar(32)

		-- Only these character can be used for the codes
		set @validTokens = '34679abcdefghijkmnpqrtwxyACDEFGHJKLMNPQRTWXY'
		set @lenValidTokens = len(@validTokens)

		-- Generate codes in a loop, generate some extra to get enough unique codes
		set @voucherCount = 0
		while @voucherCount < @nbrOfVouchers + @nbrOfVouchers / 10 + 10
		begin
			-- Generate code
			set @tokenCount = 0
			set @code = ''
			set @sum = 0
			while @tokenCount < @nbrOfToken
			begin
				set @pos = convert(int, rand() * @lenValidTokens)
				set @token = substring(@validTokens, @pos + 1, 1)
				set @sum = @sum + ascii(@token)
				set @code = @code + @token

				set @tokenCount = @tokenCount + 1
			end

			-- Append checksum
			set @sum = @sum % 100
			if @sum < 10 set @code = @code + '0'
			set @code = @code + convert(varchar(2), @sum)

			-- Build the final code
			set @code = @prefix + @code + @suffix

			-- Insert the code in our temporary amount
			insert into
				@codes
				(code)
			values
				(@code)

			set @voucherCount = @voucherCount + 1
		end
		
		
		-- Valid-to-date
		if @ValidTo is null
		begin
			declare @ValidInDays int
			select
				@ValidInDays = ValidInDays
			from
				[product].tVoucherInfo
			where
				VoucherInfoId = @VoucherInfoId
			
			-- If @ValidTo is null then set to today + ValidInDays in tVoucherInfo (23:59).
			-- If ValidInDays is null as well then set ValidTo = null for the generated vouchers.
			if @ValidInDays is not null
			begin
				set @ValidTo = cast((convert(varchar(8), dateadd(day, @ValidInDays, getdate()), 112) + ' 23:59') as smalldatetime)
			end
		end

		-- Table to save generated id's in.
		declare @Vouchers table (VoucherId int)

		-- Insert the generated codes
		insert into
			[product].[tVoucher]
			(VoucherInfoId, VoucherCode, ValidTo, QuantityLeft, AmountLeft) -- lägg till id
		output inserted.VoucherId into @Vouchers
		-- Pick the required number of codes
		-- Distinct avoids picking duplicates in the generated amount
		select distinct
			top (@nbrOfVouchers)
			@VoucherInfoId,
			code,
			@ValidTo,
			@OriginalQuantity,
			@DiscountValue
		from
			@codes
		-- Only insert codes that doesn't exist
		where
			code not in (
				select
					VoucherCode
				from
					[product].[tVoucher]
			)


		-- Handle customers
		if @customers is not null
		begin
			declare @CustomerIds table (CustomerId int)

			insert into
				@CustomerIds
				(CustomerId)
			select
				T.c.value('@id', 'int') as CustomerId
			from
				@customers.nodes('customers/customer') as T(c)


			-- Check that input xml contains the right number of rows.
			if @@rowcount <> @nbrOfVouchers
			begin
				raiserror ('Supplied number of customers must match the number of codes to generate.', 16, 1)
			end

			-- Table to save generated codes for customers
			declare @VoucherCustomers table (VoucherId int, CustomerId int)

			insert into
				product.tVoucherCustomer
				(VoucherId, CustomerId)  -- (VoucherId, CustomerId, IsUsed)
			output inserted.VoucherId, inserted.CustomerId into @VoucherCustomers -- inserted.VoucherId, inserted.CustomerId into @VoucherCustomers
			select
				V.VoucherId,
				C.CustomerId
				--0
			from (select
					VoucherId,
					row_number() over (order by VoucherId) as Row
				from @Vouchers) as V
				inner join (select
						CustomerId,
						row_number() over (order by CustomerId) as Row
					from @CustomerIds) as C
					on V.Row = C.Row
		end
		

		-- Verify usage count
		declare @Quantity int
		
		select @Quantity = Quantity from product.tVoucherInfo where VoucherInfoId = @VoucherInfoId

		if @Quantity > 0
		BEGIN
			IF ((SELECT COUNT(*) FROM product.tVoucher WHERE VoucherInfoId = @VoucherInfoId) > @Quantity)
			BEGIN
				PRINT 'The number of codes may not exceed the quantity value.'
				RAISERROR ('The number of codes may not exceed the quantity value.', 16, 1)
			END
		END

		
		-- Return info about created codes
		if @customers is not null
		begin
			-- Return info about created codes for customers
			select
				V.VoucherCode,
				VC.CustomerId
			from product.tVoucher V
				inner join @VoucherCustomers VC on V.VoucherId = VC.VoucherId
		end
		else
		begin
			-- Return info about created codes for customers
			select
				V.VoucherCode,
				null as CustomerId
			from product.tVoucher V
				inner join @Vouchers VC on V.VoucherId = VC.VoucherId
		end


		COMMIT TRANSACTION

		RETURN 0 
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()
		
		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', @ErrMsg, GETDATE(), @SP)
		
		--exec generic.pErrorRaise @ErrMsg, @SP, @Severity, @State      
		
		RETURN 1
	END CATCH		
END




--exec [backoffice].[pVoucherGenerateCodes] 3,5,null,null 
--exec [backoffice].[pVoucherGenerateCodes] 3,null,'<customers><customer id="1"/></customers>',null 

/*
insert into product.tDiscountType (DiscountType) values ('Test discount')
insert into product.tSite (SiteTitle, Description) values ('LekMer.se', '')

exec [backoffice].[pVoucherGenerateCodes] 3,5,null,null 


insert into product.tVoucherInfo (ValidFrom, ValidTo,
 Prefix, Suffix, Quantity, DiscountTypeId, 
 DiscountValue, IsActive, SiteId, Description, 
 DiscountTitle, NbrOfToken, OriginalStartQuantityPerCode)
values (
	'2010-08-12 00:00:00',
	'',
	'',
	'',
	100, -- Quantity hur många koder som ska skapas
	1,
	20,
	1,
	6,
	'Grand Opening',
	'Fredagen innan Grand Opening',
	8,
	1,
	8
)

exec [backoffice].[pVoucherGenerateCodes] 15,100,null,null 

insert into product.tVoucherInfo (ValidFrom, --ValidTo
 Prefix, Suffix, Quantity, DiscountTypeId, 
 DiscountValue, IsActive, ChannelGroupId, Description, 
 DiscountTitle, NbrOfToken, OriginalStartQuantityPerCode, ValidInDays, CreatedBy)
values (
	'2011-03-08 00:00:00',
	--'',
	'',
	'',
	100, -- Quantity hur många koder som ska skapas
	1,
	20,
	1,
	6,
	'Grand Opening',
	'Fredagen innan Grand Opening',
	8,
	1,
	8,
	'dino.miralem@it.cdon.com'
)
*/

GO
