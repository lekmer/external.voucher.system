USE [Voucher]
GO
/****** Object:  StoredProcedure [backoffice].[pSqlJobVoucherRelease]    Script Date: 07/12/2011 07:18:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [backoffice].[pSqlJobVoucherRelease]	
AS
begin
	set nocount on
	begin try
	
	begin transaction
			
			-- V�nta till augusti, snacka med Hamid
			
			-- kolla p� avbrutna k�p status 7 f�r b�da bolagen, h�mta koderna som brukades
			-- och avreservera dem
			select * 
			from lekmer.[order].tOrder o
			inner join product.tVoucherLog l
				on l.OrderId = o.OrderId
			inner join product.tVoucher v
				on v.VoucherId = l.VoucherId
			inner join product.tVoucherInfo vi
				on vi.VoucherInfoId = v.VoucherInfoId
			where 
				o.OrderStatusId = 7
				and vi.VoucherInfoId between 1 and 4
			
			
			-- select with no lock f�r att se om den beh�ver k�ras, utanf�r transactionen
			if ((select 1 from product.tVoucher with (nolock) where Reserved = 1) = 1)
				begin		
				
				update
					product.tVoucher
				set
					Reserved = 0
				where
					QuantityLeft >= 1
					and Reserved = 1
					--and ReservedTime + X min < GETDATE()
			end
			
	end try
	begin catch
		
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
	
		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
		
	end catch		 
end
